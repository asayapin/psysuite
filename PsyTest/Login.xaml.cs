﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using meet.app.netcore.Extensions;
using meet.common;
using meet.models.Auth;
using meet.models.Db;
using meet.wpf.controls.Controls;
using meet.wpf.controls.Controls.AnswerControls;
using meet.wpf.controls.Windows;
using Panel = meet.app.netcore.Windows.Panel;

namespace meet.app.netcore
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class Login
    {
        internal static User CurrentUser;
        private readonly UsersProvider _usersProvider = new UsersProvider(UserRole.Admin);
        internal static UserRole Role { get; private set; }
        static Login()
        {
            // NOTE : entry-point for code-side localization
            DrawingInput.Answer = FreeAnswerInput.Answer = Properties.Resources.uiConst_answer;
            InputBox.Ok = MultiSelect.Ok = Properties.Resources.uiConst_Ok;
            InputBox.Cancel = MultiSelect.Cancel = Properties.Resources.uiConst_Cancel;
            InputBox.Loading = MultiSelect.Loading = Properties.Resources.ui_InputBox_loading;
            DrawingInput.Pencil = Properties.Resources.ui_DrawingInput_pencil;
            DrawingInput.Erase = Properties.Resources.ui_DrawingPreview_erase;
            DrawingInput.Clear = Properties.Resources.uiConst_clear;
            ListPresenter.Add = Properties.Resources.uiConst_add;
            ListPresenter.Remove = Properties.Resources.uiConst_remove;
            ListPresenter.Clear = Properties.Resources.uiConst_clear;
            Extensions.Extensions.AnyFilter = Properties.Resources.filter_anyFile;

            meet.models.Resources.Load(typeof(Properties.Resources));

            WordExportExtensions.Stages.Add(0, Properties.Resources.ui_ResultProxy_waiting);
            WordExportExtensions.Stages.Add(1, Properties.Resources.ui_ResultProxy_exporting);
            WordExportExtensions.Stages.Add(2, Properties.Resources.ui_ResultProxy_saving);
            WordExportExtensions.Stages.Add(3, Properties.Resources.ui_ResultProxy_done);
        }

        public Login() => InitializeComponent();

        private static readonly List<UserRole> roles = new List<UserRole> { UserRole.Admin, UserRole.Doctor,};

        private void clickLogin(object sender, RoutedEventArgs e)
        {
            if (ulong.TryParse(tbPasswd.Text.Replace("-", ""), out var token))
            {
                var (role, expire, id) = token.DecryptKey();
                if (expire > DateTime.UtcNow)
                {
                    var timeout = expire - DateTime.UtcNow;
                    CurrentUser = _usersProvider.Find(id) ?? new User("demo-0", "demo user", role);
                    Role = role;

                    Dispatcher?.Invoke(() => {
                        new Panel().Show();
                        Close();
                    });

                    Task.Delay(timeout).ContinueWith(_ => Dispatcher?.Invoke(Logout));
                }
            }
        }

        private void Logout()
        {
            var windows = App.Current.Windows.OfType<Window>().Where(x => x.GetType()?.Namespace?.StartsWith("Psy") == true).ToList();
            new Login().Show();
            foreach (Window window in windows) window.Close();
        }
        
        private static readonly Regex Regex = new Regex("(.{1,4})");
        
        private void TbPasswd_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var text = tbPasswd.Text.Replace("-", "");
            tbPasswd.Text = string.Join("-", Regex.Split(text).Where(x => !string.IsNullOrWhiteSpace(x)));
            tbPasswd.CaretIndex = tbPasswd.Text.Length;
        }
    }
}
