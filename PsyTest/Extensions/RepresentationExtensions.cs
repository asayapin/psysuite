﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms.Integration;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.Methodical.Representational;
using meet.models.PassResults;
using Microsoft.Win32;
using Resources = meet.app.netcore.Properties.Resources;

namespace meet.app.netcore.Extensions
{
    public static class RepresentationExtensions
    {
        public static void SaveAsPicture(this GraphicalRepresentation repr, Methodic m, MethodicResult mr, MethodicResultService dp, string path, ChartImageFormat format = ChartImageFormat.Png)
        {
            var axisX = repr.Values.ToArray();
            var axisY = repr.Values.Select(x => m.Representation.Representations.Find(y => y.Name == x))
                .OfType<PlainRepresentation>().Select(x => x.Value(m, mr, dp)).ToArray();
            var cht = new Chart { Tag = repr.Name };

            cht.ChartAreas.Add("default");
            cht.Series.Add("s0");
            cht.Series[0].ChartArea = "default";
            cht.Series[0].ChartType = (SeriesChartType)repr.GraphType;
            cht.Series[0].Points.DataBindXY(axisX, axisY);

            cht.SaveImage(path, format);
        }

        public static UIElement Build(this GraphicalRepresentation repr, Methodic m, MethodicResult mr, MethodicResultService dp)
        {
            var wfh = new WindowsFormsHost();
            var axisX = repr.Values.ToArray();
            var axisY = repr.Values.Select(x => m.Representation.Representations.Find(y => y.Name == x))
                .OfType<PlainRepresentation>().Select(x => x.Value(m, mr, dp)).ToArray();

            var cht = new Chart { Tag = repr.Name };

            cht.ChartAreas.Add("default");
            cht.Series.Add("s0");
            cht.Series[0].ChartArea = "default";
            cht.Series[0].ChartType = (SeriesChartType)repr.GraphType;
            cht.Series[0].Points.DataBindXY(axisX, axisY);

            var ctx = new System.Windows.Forms.ContextMenu();
            var mi = ctx.MenuItems.Add(Resources.title_GraphicalRepresentation_charCtxSave);
            mi.Click += delegate {
                var sfd = new SaveFileDialog
                {
                    Filter = Resources.filter_GraphicalRepresentation_savePics,
                    OverwritePrompt = true,
                    Title = string.Format(Resources.format_GraphicalRepresentation_saveTitle, repr.Name),
                    AddExtension = true,
                    FileName = repr.Name
                };
                if (sfd.ShowDialog() == true)
                {
                    var cif = ChartImageFormat.Png;
                    switch (sfd.FilterIndex)
                    {
                        case 2: cif = ChartImageFormat.Jpeg; break;
                        case 3: cif = ChartImageFormat.Bmp; break;
                    }
                    cht.SaveImage(sfd.FileName, cif);
                }
            };
            cht.ContextMenu = ctx;

            wfh.Child = cht;

            return wfh;
        }
        public static IEnumerable<UIElement> Build(this Representation repr, Methodic m, MethodicResult mr, MethodicResultService dp) => repr.Representations.Select(x => x.Build(m, mr, dp));

        public static UIElement Build(this SubRepresentation repr, Methodic m, MethodicResult mr, MethodicResultService dp)
        {
            if (repr is PlainRepresentation p) return p.Build(m, mr, dp);
            if (repr is GraphicalRepresentation g) return g.Build(m, mr, dp);
            return null;
        }
        public static UIElement Build(this PlainRepresentation repr, Methodic m, MethodicResult mr, MethodicResultService dp)
        {
            if (repr.IsVisible)
            {
                return new TextBlock
                {
                    Text = repr.Content(m, mr, dp),
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(4),
                };
            }
            return null;
        }
    }
}
