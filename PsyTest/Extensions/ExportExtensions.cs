﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using meet.app.netcore.Windows;
using meet.models;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.Methodical.Representational;
using meet.models.PassResults;
using Microsoft.Win32;
using NPOI.OpenXmlFormats.Wordprocessing;
using NPOI.Util;
using NPOI.XWPF.UserModel;

namespace meet.app.netcore.Extensions
{
    public static class ExportExtensions
    {
        public static TestResult.RawResult MakeRawResult(this Methodic m, Question q, ResultAnswer a)
        {
            var ret = new TestResult.RawResult(q.Text, a.ToString());
            if (m.AnswersType != AnswersType.Variants) return ret;
            switch (a.ResultType)
            {
                case ResultType.Variant:
                    ret.Answer = string.Format(Properties.Resources.format_TestResult_answerView, a.GetValue(), q.Answers[a.GetValue()]);
                    break;
                case ResultType.Text:
                    ret.Answer = Encoding.UTF8.GetString(a.Value);
                    break;
                case ResultType.Drawing:
                    ret.Answer = Properties.Resources.itemConst_drawingAnswer;
                    break;
            }
            return ret;
        }

        public static string FirstLine(this string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return s ?? "";
            return s.Split(new[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries).ElementAtOrDefault(0);
        }

        public static string GetPath(Methodic m, MethodicResult mr, bool silentSave, DateTimeOffset pt)
        {
            var s = string.Format(Properties.Resources.format_TestResult_exportItem, mr.Patient, m, pt);
            s = s.SubstitutePathChars();
            if (silentSave) return s + ".doc";
            var sfd = new SaveFileDialog
            {
                Filter = Properties.Resources.filter_TestResults_export,
                Title = Properties.Resources.title_TestResults_export,
                FileName = s
            };
            if (sfd.ShowDialog() == true)
            {
                return sfd.FileName;
            }
            return s + ".doc";
        }
    }
}
