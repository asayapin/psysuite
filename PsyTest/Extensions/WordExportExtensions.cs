﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.Methodical.Representational;
using meet.models.PassResults;
using NPOI.Util;
using NPOI.XWPF.UserModel;

namespace meet.app.netcore.Extensions
{
    public static class WordExportExtensions
    {
        public static Dictionary<int, string> Stages = new Dictionary<int, string>();

        public static string ExportToWord(this MethodicResult mr, Methodic m, Action<bool?, string, int?> onStageChange,
            MethodicResultService service, bool silentSave, bool exportAnswers, string exportPath = null)
        {
            var pt = new DateTimeOffset(mr.PassTimestamp, TimeSpan.FromMinutes(mr.UtcOffset));

            var document = new XWPFDocument();

            Write(m, mr, onStageChange, pt, service, exportAnswers, document);

            onStageChange?.Invoke(true, Stages[2], null);

            var path = ExportExtensions.GetPath(m, mr, silentSave, pt);
            if (!string.IsNullOrWhiteSpace(exportPath))
                path = Path.Combine(exportPath, Path.GetFileName(path));

            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write)) document.Write(fs);

            onStageChange?.Invoke(null, Stages[3], null);
            return exportPath ?? Directory.GetCurrentDirectory();
        }
        private static void Write(
            Methodic m, MethodicResult result, Action<bool?, string, int?> onStageChange,
            DateTimeOffset pt, MethodicResultService service, bool exportRaw, XWPFDocument document)
        {
            if (result.Patient is null) return;
            onStageChange?.Invoke(null, Stages[1], null);

            AddParagraph(string.Format(Properties.Resources.format_TestResult_exportPatient, result.Patient), document);
            AddParagraph(string.Format(Properties.Resources.format_TestResult_exportMethodic, m.Title), document);
            AddParagraph(string.Format(Properties.Resources.format_TestResult_exportPassedOn, pt), document);

            if (exportRaw || !m.Representation.Representations.OfType<PlainRepresentation>().Any(x => x.IsVisible))
            {
                WriteTable(result, m, document);
            }

            foreach (PlainRepresentation prep in m.Representation.Representations.OfType<PlainRepresentation>()
                .Where(x => x.IsVisible))
            {
                onStageChange?.Invoke(null, null, 1);
                AddParagraph(prep.Content(m, result, service), document);
            }

            foreach (GraphicalRepresentation grep in m.Representation.Representations.OfType<GraphicalRepresentation>())
            {
                var p = Path.GetTempFileName();
                grep.SaveAsPicture(m, result, service, p);
                InsertImage(new MemoryStream(File.ReadAllBytes(p)), grep.Name, document);
                File.Delete(p);
                onStageChange?.Invoke(null, null, 1);
            }
        }

        private static void WriteTable(MethodicResult result, Methodic m, XWPFDocument document)
        {
            var table = document.CreateTable();

            var row = table.GetRow(0);
            SetCell(row, Properties.Resources.header_TestResult_question, 0);
            SetCell(row, Properties.Resources.header_TestResult_answer, 1);

            foreach (var raw in m.Questions.Zip(result.Answers, m.MakeRawResult).ToList())
            {
                row = table.CreateRow();
                SetCell(row, raw.Question.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ElementAtOrDefault(0), 0);
                SetCell(row, raw.Answer, 1);
            }
        }

        private static void SetCell(XWPFTableRow row, string text, int index)
        {
            (row.GetCell(index) ?? row.CreateCell()).AddParagraph().CreateRun().SetText(text);
        }

        private static void AddParagraph(string text, XWPFDocument document)
        {
            document.CreateParagraph().CreateRun().SetText(text);
        }

        private static void InsertImage(Stream stream, string name, XWPFDocument document)
        {
            var para = document.CreateParagraph();
            var run = para.CreateRun();
            run.AddPicture(stream, (int)PictureType.PNG, name, Units.ToEMU(250), Units.ToEMU(250));
        }
    }
}
