﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using meet.models;
using meet.models.Methodical;
using meet.wpf.controls;
using meet.wpf.controls.Controls.AnswerControls;

namespace meet.app.netcore.Extensions
{
    public static class UiExtensions
    {
        public static Control UiRepresentation(this Answer answer, Action<object, EventArgs> callback)
        {
            switch (answer.AnswerType)
            {
                case AnswerType.Text:
                case AnswerType.Picture:
                    var btn = new Button
                    {
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center,
                        Margin = new Thickness(12),
                        Padding = new Thickness(8)
                    };
                    btn.Click += new RoutedEventHandler(callback);
                    if (answer.AnswerType == AnswerType.Text)
                        btn.Content = new TextBlock { Text = Encoding.UTF8.GetString(answer.Data) };
                    else
                    {
                        var bi = answer.Data?.AsImage();
                        if (bi is null)
                            MessageBox.Show(Resources.error_Answer_conversionFailure);
                        else btn.Content = new Image { Source = bi };
                    }

                    return btn;
                case AnswerType.FreeText:
                    var fai = new FreeAnswerInput
                    {
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center,
                        Margin = new Thickness(24),
                        Padding = new Thickness(8)
                    };
                    fai.AnswerObtained += new EventHandler<ResultEventArgs>(callback);
                    return fai;
                case AnswerType.Drawing:
                    var di = new DrawingInput
                    {
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Stretch,
                        Margin = new Thickness(24),
                        Padding = new Thickness(8)
                    };
                    di.AnswerObtained += new EventHandler<ResultEventArgs>(callback);
                    return di;
                default: return null;
            }
        }

        public static BitmapImage AsImage(this byte[] src)
        {
            try
            {
                if (src is null || src.Length == 0) return null;
                var bi = new BitmapImage();
                using (var ms = new MemoryStream(src))
                {
                    ms.Position = 0;
                    bi.BeginInit();
                    bi.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                    bi.CacheOption = BitmapCacheOption.OnLoad;
                    bi.UriSource = null;
                    bi.StreamSource = ms;
                    bi.EndInit();
                }
                bi.Freeze();
                return bi;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return null;
        }

        public static IEnumerable<T> GetChildren<T>(this UIElement source)
        {
            switch (source)
            {
                case ItemsControl ic:
                    foreach (UIElement a in ic.Items)
                    foreach (var b in GetChildren<T>(a))
                        yield return b;
                    break;

                case ContentControl cc when cc.Content is UIElement uie:
                    foreach (var a in GetChildren<T>(uie))
                        yield return a;
                    break;
                case System.Windows.Controls.Panel panel:
                    foreach (UIElement child in panel.Children)
                    foreach (var c in GetChildren<T>(child))
                        yield return c;
                    break;
            }
            if (source is T t)
                yield return t;
        }
    }
}
