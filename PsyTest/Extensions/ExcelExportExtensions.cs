﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using meet.models.Db;
using meet.models.Methodical.Representational;
using meet.models.PassResults;
using Microsoft.Win32;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace meet.app.netcore.Extensions
{
    public static class ExcelExportExtensions
    {
        public static string ExportToExcel(this IEnumerable<MethodicResult> results, MethodicsService mService, MethodicResultService mrService, bool exportRaw)
        {
            var res = results.GroupBy(x => x.MethodicName).ToDictionary(
                x => x.Key,
                x => x.GroupBy(g => g.Patient.User.Identifier)
                    .ToDictionary(
                        g => g.Key,
                        g => g.ToList()));

            var workbook = new XSSFWorkbook();

            res.WriteTable(mService, mrService, exportRaw, workbook);

            var sfd = new SaveFileDialog
            {
                Filter = Properties.Resources.filter_TestResults_exportTable,
                Title = Properties.Resources.title_TestResults_export,
                FileName = $"Export results - {DateTimeOffset.Now.ToString().SubstitutePathChars()}",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            };
            if (sfd.ShowDialog() != true) return "";

            using (var fs = new FileStream(sfd.FileName, FileMode.Create, FileAccess.Write)) workbook.Write(fs);

            return sfd.FileName;
        }

        private static void WriteTable(this Dictionary<string, Dictionary<string, List<MethodicResult>>> resultsGroups,
            MethodicsService mService, MethodicResultService mrService, bool exportRaw, XSSFWorkbook workbook)
        {
            foreach (var (methodicName, dictionary) in resultsGroups)
            {
                var methodic = mService.Find(x => x.Title == methodicName).Single();

                var plains = methodic.Representation.Representations.OfType<PlainRepresentation>().Where(x => x.IsVisible).ToList();

                var sheet = workbook.CreateSheet(methodicName);

                WriteAt(0, 0, Properties.Resources.header_exported_patientColumn, sheet);
                WriteAt(0, 1, Properties.Resources.header_exported_timestampColumn, sheet);

                var names = plains.Select(x => x.Name).ToList();
                if (names.Count == 0 || exportRaw)
                    names.AddRange(methodic.Questions.Select(x => x.Text.FirstLine()));
                WriteHeader(2, names, sheet);

                var line = 1;

                foreach (var (patientId, results) in dictionary)
                {
                    foreach (var result in results)
                    {
                        WriteAt(line, 0, patientId, sheet);
                        WriteAt(line, 1, result.GetPassTime(), sheet);

                        var data = plains.Select(p => p.Value(methodic, result, mrService).ToString("0.##")).ToList();
                        if (data.Count == 0 || exportRaw)
                            data.AddRange(methodic.Questions.Zip(result.Answers, methodic.MakeRawResult).Select(rr => rr.Answer));

                        var col = 2;
                        foreach (var s in data) WriteAt(line, col++, s, sheet);

                        line++;
                    }
                }
            }
        }

        private static void WriteAt(int r, int c, object val, ISheet sheet)
        {
            var row = sheet.GetRow(r) ?? sheet.CreateRow(r);
            var cell = row.GetCell(c) ?? row.CreateCell(c);
            cell.SetCellValue(val.ToString());
        }

        private static void WriteHeader(int c, IEnumerable<string> names, ISheet sheet)
        {
            foreach (var name in names) WriteAt(0, c++, name, sheet);
        }
    }
}
