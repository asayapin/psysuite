﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.Win32;

namespace meet.app.netcore.Extensions
{
    public static class Extensions
    {
        public static string AnyFilter { get; set; }

        public static IEnumerable<Exception> FlattenInners(this Exception ex)
        {
            if (!(ex.InnerException is null))
            {
                yield return ex.InnerException;
                foreach (var iex in ex.InnerException.FlattenInners())
                    yield return iex;
            }
            yield return ex;

        }
        public static int Up(this IList list, int position)
        {
            if (position <= 0 || position >= list.Count) return position;
            var tmp = list[position];
            list.RemoveAt(position);
            list.Insert(position - 1, tmp);
            return position - 1;
        }
        public static int Down(this IList list, int position)
        {
            if (position < 0 || position >= list.Count - 1) return position;
            var tmp = list[position];
            list.RemoveAt(position);
            list.Insert(position + 1, tmp);
            return position + 1;
        }
        public static OpenFileDialog AsOpenFileDialog(this string title, string filters)
        {
            return new OpenFileDialog
            {
                Title = title,
                AddExtension = true,
                CheckFileExists = true,
                CheckPathExists = true,
                Filter = filters ?? AnyFilter,
                Multiselect = false,
            };
        }

        public static void OpenExplorer(this string path) => new Process {StartInfo = new ProcessStartInfo(Properties.Settings.Default.explorerPath, path)}.Start();

        public static string SubstitutePathChars(this string path) => Path.GetInvalidFileNameChars()
            .Concat(Path.GetInvalidPathChars()).Distinct()
            .Aggregate(path, (agg, c) => agg.Replace(c, '_'));
    }
}
