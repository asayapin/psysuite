﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using meet.app.netcore.Extensions;
using meet.models.Methodical;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для EditAnswer.xaml
    /// </summary>
    public partial class EditAnswer : Window
    {
        public Answer Binder { get; set; } = new Answer();
        private readonly Answer Source = new Answer();
        private bool exitState;
        public EditAnswer()
        {
            InitializeComponent();
            cbType.ItemsSource = Enum.GetValues(typeof(AnswerType));
        }

        public EditAnswer(Answer answer) : this()
        {
            Source = answer;
            Binder.Load(Source);
            if (Binder.AnswerType == AnswerType.Text)
                tbData.Text = Encoding.UTF8.GetString(Source.Data);
            cbType.SelectedItem = Binder.AnswerType;
        }

        private void clickSave(object sender, RoutedEventArgs e)
        {
            exitState = true;
            Close();
        }

        private void EditAnswer_OnClosing(object sender, CancelEventArgs e)
        {
            if (exitState)
                Source.Load(Binder);
        }

        private void rebuildBinder()
        {
            switch (Binder.AnswerType) {
                case AnswerType.Text:
                    Binder.Data = Encoding.UTF8.GetBytes(tbData.Text);
                    break;
                case AnswerType.Picture:
                    if (File.Exists(tbData.Text))
                    Binder.Data = File.ReadAllBytes(tbData.Text);
                    break;
            }
            grResult.Children.Clear();
            grResult.Children.Add(Binder.UiRepresentation((s, e) => { }));
        }

        private void clickBrowse(object sender, RoutedEventArgs e)
        {
            var ofd = Properties.Resources.title_EditAnswer_instructionsOFD.AsOpenFileDialog(Properties.Resources.filter_EditAnswer_bitmapPics);
            if (ofd.ShowDialog() != true) return;
            Binder.Data = File.ReadAllBytes(ofd.FileName);
            tbData.Text = ofd.FileName;
            rebuildBinder();
        }

        private void TbData_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Binder.Data = Encoding.UTF8.GetBytes(tbData.Text);
            rebuildBinder();
        }

        private void CbType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Binder.AnswerType = (AnswerType)cbType.SelectedItem;
            btBrowse.Visibility = Binder.AnswerType == AnswerType.Picture ? Visibility.Visible : Visibility.Collapsed;
            rebuildBinder();
        }
    }
}
