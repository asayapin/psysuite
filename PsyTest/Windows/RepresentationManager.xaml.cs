﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using meet.app.netcore.Extensions;
using meet.models;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.Methodical.Representational;
using meet.models.PassResults;
using meet.wpf.controls.Controls;
using meet.wpf.controls.Windows;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для RepresentationManager.xaml
    /// </summary>
    public partial class RepresentationManager : Window
    {
        internal event EventHandler<bool> RepUpdated;

        private readonly Representation source = new Representation();
        public PlainRepresentation Plain { get; set; } = new PlainRepresentation();
        private readonly GraphicalRepresentation graph = new GraphicalRepresentation();
        private readonly Methodic msrc = new Methodic();
        private readonly Methodic buildm = new Methodic();
        private MethodicResult testResult;

        private readonly List<string> questions = new List<string>();
        private readonly MethodicResultService _resultProvider = new MethodicResultService(Login.Role);

        private bool? current;
        private SubRepresentation Current => current == true ? Plain : current == false ? (SubRepresentation)graph : null;

        public RepresentationManager()
        {
            InitializeComponent();
            RepUpdated += ReprUpdateHandler;
        }

        private void ReprUpdateHandler(object sender, bool e)
        {
            try {
                var ctl = Current.Build(buildm, testResult, _resultProvider);
                if (ctl is null) return;
                if (e) {
                    Plain.Name = tbName.Text;
                    grResult.Children.Clear();
                    grResult.Children.Add(ctl);
                } else {
                    graph.Name = tbName.Text;
                    grGraphPreview.Children.Clear();
                    grGraphPreview.Children.Add(ctl);
                }
            } catch (Exception ex) {
                Debug.WriteLine(ex.Message);
            }
        }

        private void createResult(Methodic m)
        {
            var r = new Random();
            int cap = -1;
            switch (m.AnswersType) {
                case AnswersType.None:
                    return;
                case AnswersType.YesNo:
                    cap = 2;
                    break;
                case AnswersType.Scale:
                    cap = (int)m.ScaleWidth;
                    break;
            }
            testResult = new MethodicResult();
            var i = 0;
            foreach (var q in m.Questions)
                testResult.Answers.Add(new ResultAnswer {
                    Question = i++,
                    Value = BitConverter.GetBytes(r.Next(cap == -1 ? q.Answers.Count : cap))
                });
        }

        public RepresentationManager(Methodic m) : this()
        {
            msrc = m;
            buildm.Load(m);
            buildm.Representation = source;
            source.Load(m.Representation);
            createResult(m);

            lpSubs.CreateProvider = () => new SubRepresentation();
            lpGValues.CreateProvider = () => MultiSelect.ShowDialog(Properties.Resources.title_RepresentationManager_valueSourceGR, source.Representations.OfType<PlainRepresentation>().Select(x => x.Name));
            lpPTags.CreateProvider = () => MultiSelect.ShowDialog(Properties.Resources.title_RepresentationManager_selectTag, m.Questions.SelectMany(x => x.Tags).Distinct());
            lpPQuestions.CreateProvider = () => MultiSelect.ShowDialog(Properties.Resources.title_RepresentationManager_selectQuestion, m.Questions.Select(x => x.Text));

            lpSubs.ItemsSource = source.Representations;
            lpGValues.ItemsSource = graph.Values;
            lpPQuestions.ItemsSource = questions;
            lpPTags.ItemsSource = Plain.Tags;

            cbMatchType.ItemsSource = Enum.GetValues(typeof(MatchType));
            cbGGType.ItemsSource = Enum.GetValues(typeof(SeriesChartType));
            cbGSType.ItemsSource = Enum.GetValues(typeof(ScaleType));
        }

        private void changedRepresentationType(object sender, SelectionChangedEventArgs e)
        {
            if ((cbRType.SelectedItem as ComboBoxItem)?.Content as string == Properties.Resources.ui_RepresentationManager_graphCB)
            {
                current = false;
                grPlain.Visibility = Visibility.Collapsed;
                grGraph.Visibility = Visibility.Visible;
            }
            else if ((cbRType.SelectedItem as ComboBoxItem)?.Content as string == Properties.Resources.ui_RepresentationManager_plainCB)
            {
                current = true;
                grPlain.Visibility = Visibility.Visible;
                grGraph.Visibility = Visibility.Collapsed;
            }
            else
            {
                current = null;
                grPlain.Visibility = Visibility.Collapsed;
                grGraph.Visibility = Visibility.Collapsed;
            }
        }

        private void clickCommitChanges(object sender, RoutedEventArgs e)
        {
            var cc = Current;
            if (cc is PlainRepresentation pr)
                if (pr.RequiredQuestions(msrc).Count() != PatternItem.SafeParse(pr.Pattern).Count && pr.MatchType != MatchType.ByValue) {
                    MessageBox.Show(Properties.Resources.error_RepresentationManager_commitError);
                    return;
                }
            cc.Name = tbName.Text;
            source.Representations[lpSubs.SelectedIndex] = (SubRepresentation)cc.Copy();
            lpSubs.RefreshView();
        }

        private void LpPQuestions_OnSourceChanged(object sender, ListPresenter.EventArgs e)
        {
            switch (e.Type) {
                case ListPresenter.EventType.Add:
                    Plain.Questions.Add(msrc.Questions.FindIndex(x => x.Text == questions.Last()));
                    break;
                case ListPresenter.EventType.Remove:
                    Plain.Questions.RemoveAt(e.Index);
                    break;
                case ListPresenter.EventType.Clear:
                    Plain.Questions.Clear();
                    break;
            }
            updateSelectedQuestions();
        }

        private void CbMatchType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Plain.MatchType = (MatchType)cbMatchType.SelectedItem;
            RepUpdated?.Invoke(null, true);
        }

        private void TbPattern_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Plain.Pattern = tbPattern.Text;
            RepUpdated?.Invoke(null, true);
        }

        private void LpPTags_OnSourceChanged(object sender, ListPresenter.EventArgs e)
        {
            updateSelectedQuestions();
        }

        private void updateSelectedQuestions()
        {
            tbUsedQuestions.Text = string.Join(" ", Plain.RequiredQuestions(msrc));
            RepUpdated?.Invoke(null, true);
        }

        private void CbGGType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            graph.GraphType = (int)cbGGType.SelectedItem;
            RepUpdated?.Invoke(null, false);
        }

        private void CbGSType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            graph.ScaleType = (ScaleType)cbGSType.SelectedItem;
            RepUpdated?.Invoke(null, false);
        }

        private void clickSave(object sender, RoutedEventArgs e)
        {
            msrc.Representation = source;
            Close();
        }

        private void LpSubs_OnItemDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            switch (lpSubs.SelectedItem) {
                case PlainRepresentation pr:
                    tbName.Text = pr.Name;
                    Plain.Load(pr);
                    cbRType.SelectedIndex = 0;
                    cbMatchType.SelectedItem = pr.MatchType;
                    tbPattern.Text = string.Join(" ", pr.Pattern);
                    lpPQuestions.RefreshView();
                    lpPTags.RefreshView();
                    updateSelectedQuestions();
                    break;
                case GraphicalRepresentation gr:
                    tbName.Text = gr.Name;
                    graph.Load(gr);
                    cbRType.SelectedIndex = 1;
                    cbGGType.SelectedItem = gr.GraphType;
                    cbGSType.SelectedItem = gr.ScaleType;
                    lpGValues.RefreshView();
                    break;
                default:
                    cbRType.SelectedIndex = -1;
                    tbName.Text = "";
                    // TODO : clear UI
                    break;
            }
            RepUpdated?.Invoke(null, true);
        }

        private void ChangedPostprocess(object sender, TextChangedEventArgs e)
        {
            Plain.PostProcess = (sender as TextBox).Text;
            RepUpdated?.Invoke(null, true);
        }
    }
}
