﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using meet.app.netcore.Extensions;
using meet.common;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.PassResults;
using meet.wpf.controls.Windows;
using MessageBox = System.Windows.MessageBox;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для Panel.xaml
    /// </summary>
    public partial class Panel : Window
    {
        private readonly MethodicsService _methodic = new MethodicsService(Login.Role);
        private readonly MethodicResultService _result = new MethodicResultService(Login.Role);
        private readonly UsersProvider _users = new UsersProvider(Login.Role);
        public Panel()
        {
            InitializeComponent();
            miCurrentRole.Header = string.Format(Properties.Resources.format_Panel_welcome, Login.CurrentUser.Name);
            if (Login.CurrentUser.Role == UserRole.Admin)
            {
                miAdmin.Visibility = Visibility.Visible;
                tbHeader.Text = Properties.Resources.ui_Panel_adminDGHeader;
            }
            else
            {
                miAdmin.Visibility = Visibility.Collapsed;
                miMehodics.Visibility = Visibility.Collapsed;
            }

            reloadResults();

            miEn.IsChecked = !(miRu.IsChecked = Thread.CurrentThread.CurrentUICulture.Name.StartsWith("ru"));
        }

        private void reloadResults()
        {
            grLoading.Visibility = Visibility.Visible;
            Task.Run(() =>
            {
                Dispatcher?.InvokeAsync(() =>
                {
                    try
                    {
                        dgPassedTests.ItemsSource = _result.GetItems();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                    grLoading.Visibility = Visibility.Collapsed;
                });
            });
        }
        private void clickMethodics(object sender, RoutedEventArgs e)
        {
            if (Login.CurrentUser.Role == UserRole.Admin)
            {
                new EditTest().ShowDialog();
            }
            else
            {
                new MethodicsPreview().Show();
                Close();
            }
        }

        private void clickRoleChange(object sender, RoutedEventArgs e)
        {
            new Login().Show();
            Close();
        }

        private void clickStartTest(object sender, RoutedEventArgs e)
        {
            new SelectPatient().Show();
            Close();
        }

        private void clickViewPatient(object sender, RoutedEventArgs e) => new PatientInfo().ShowDialog();

        private void doubleClickPassedTest(object sender, MouseButtonEventArgs e)
        {
            var tr = new TestResult(dgPassedTests.SelectedItem as MethodicResult);
            if (!tr.err)
                tr.Show();
        }

        private void clickHelp(object sender, RoutedEventArgs e)
        {
            new About().Show();
        }

        private void DgPassedTests_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.Column.Header.ToString())
            {
                case nameof(ResultPreview.Methodic):
                    e.Column.Header = Properties.Resources.header_Panel_resultsMethodic;
                    break;
                case nameof(ResultPreview.Patient):
                    e.Column.Header = Properties.Resources.header_Panel_resultsPatient;
                    break;
                case nameof(ResultPreview.Timestamp):
                    e.Column.Header = Properties.Resources.header_Panel_resultsTimestamp;
                    break;
            }
        }

        private void clickLoadMethodic(object sender, RoutedEventArgs e)
        {
            var ofd = Properties.Resources.title_Panel_loadMethodicOFD.AsOpenFileDialog(Properties.Resources.filter_Panel_xml);
            ofd.Multiselect = true;
            if (ofd.ShowDialog() != true) return;
            grLoading.Visibility = Visibility.Visible;
            Task.WhenAll(
                ofd.FileNames.Select(x => Task.Factory.StartNew(n =>
                {
                    var met = Methodic.Load(n as string);
                    if (met is null)
                    {
                        MessageBox.Show($"Failed to load: {n}");
                        return;
                    }
                    _methodic.Insert(met);
                }, x))
                ).ContinueWith(x => Dispatcher.Invoke(() => grLoading.Visibility = Visibility.Collapsed));
        }

        private void clickBatchExport(object sender, RoutedEventArgs e)
        {
            var os = new Dictionary<string, bool>
            {
                [Properties.Resources.title_Panel_exportToTable] = true,
                [Properties.Resources.title_Panel_exportRawAnswers] = false
            };
            var ms = MultiSelect.ShowDialog(Properties.Resources.title_Panel_batchMSTitle, _result.GetItems(), os);
            if (ms is null) return;
            var exportRaw = os[Properties.Resources.title_Panel_exportRawAnswers];
            if (os[Properties.Resources.title_Panel_exportToTable])
            {
                Path.GetDirectoryName(ms.ExportToExcel(_methodic, _result, exportRaw)).OpenExplorer();
            } else
                new BatchExport(ms, exportRaw).Show();

        }

        private void clickReload(object sender, RoutedEventArgs e)
        {
            reloadResults();
        }

        private void clickDumpMethodics(object sender, RoutedEventArgs e)
        {
            var ms = MultiSelect.ShowDialog(Properties.Resources.title_Panel_selectMethsToDump, _methodic.GetItems());
            if (ms is null) return;
            var fbd = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
                Description = Properties.Resources.title_Panel_fbdDumpDescription
            };
            if (fbd.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            grLoading.Visibility = Visibility.Visible;
            Task.WhenAll(
                ms.Select(x =>
                    Task.Factory.StartNew(m =>
                    {
                        if (!(m is Methodic mtd)) return;
                        mtd.Write(Path.Combine(fbd.SelectedPath, mtd.Title + ".xml"));
                    }, x)))
                .ContinueWith(x => Dispatcher.Invoke(() => grLoading.Visibility = Visibility.Collapsed));
        }

        private void clickLocEnglish(object sender, RoutedEventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            new Panel().Show();
            Close();
        }

        private void clickLocRussian(object sender, RoutedEventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru");
            new Panel().Show();
            Close();
        }
    }
}
