﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using meet.models.Auth;
using meet.models.Db;
using meet.models.PassResults;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для PatientInfo.xaml
    /// </summary>
    public partial class PatientInfo
    {
        public Patient Binder { get; set; } = new Patient{User = new User()};

        private readonly PatientsProvider _patientsProvider = new PatientsProvider(Login.Role);
        public PatientInfo()
        {
            InitializeComponent();
            loadPatients();
        }

        private void loadPatients()
        {
            grLoading.Visibility = Visibility.Visible;
            Task.Run(() =>
            {
                var ls = _patientsProvider.GetItems().ToList();
                ls.Insert(0, new Patient {User = new User{Identifier = Properties.Resources.headerConst_createItemCB}});
                Dispatcher.Invoke(() =>
                {
                    cbPatients.ItemsSource = ls;
                    grLoading.Visibility = Visibility.Collapsed;
                });
            });
        }

        private void selectedPatient(object sender, SelectionChangedEventArgs e) {
            if (!(cbPatients.SelectedItem is Patient p)) return;
            if (p.User.Identifier == Properties.Resources.headerConst_createItemCB) {
                p = CreatePatient.ShowDialog(true);
                if (p?.User?.Identifier is null) return;
                _patientsProvider.Insert(p);
                loadPatients();
            }
            reload(p);
        }

        private void reload(Patient p) {
            if (p is null) return;
            Binder.Load(p);
            grLoading.Visibility = Visibility.Visible;
            Dispatcher.InvokeAsync(() =>
            {
                _patientsProvider.WithAttached(p, patient => dgPassed.ItemsSource = patient.PassedMethodics.Select(x => x.Unwrap()).ToList());
                grLoading.Visibility = Visibility.Collapsed;
            });
        }

        private void doubleClickGridEntry(object sender, MouseButtonEventArgs e)
        {
            var tr = new TestResult(dgPassed.SelectedItem as MethodicResult);
            if (!tr.err)
                tr.Show();
        }

        private void DgPassed_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.Column.Header.ToString()) {
                case nameof(ResultPreview.Methodic):
                    e.Column.Header = Properties.Resources.header_Panel_resultsMethodic;
                    break;
                case nameof(ResultPreview.Patient):
                    e.Column.Header = Properties.Resources.header_Panel_resultsPatient;
                    break;
                case nameof(ResultPreview.Timestamp):
                    e.Column.Header = Properties.Resources.header_Panel_resultsTimestamp;
                    break;
            }
        }

        private void restoreClick(object sender, RoutedEventArgs e) {
            reload(cbPatients.SelectedItem as Patient);
        }

        private void saveClick(object sender, RoutedEventArgs e) {
            if (!(cbPatients.SelectedItem is Patient p)) return;
            p.Load(Binder);
            Task.Run(() => _patientsProvider.Update(p));
        }
    }
}
