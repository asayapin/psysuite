﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using meet.app.netcore.Extensions;
using meet.common;
using meet.models;
using meet.models.Auth;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.PassResults;
using meet.wpf.controls;
using meet.wpf.controls.Controls;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для TestPass.xaml
    /// </summary>
    public partial class TestPass
    {
        private readonly Queue<Methodic> queue = new Queue<Methodic>();
        private Methodic current;
        private Dictionary<int, ResultAnswer> answers;
        private readonly Patient patient;
        private int cindex;
        private Dictionary<string[], string> headers;

        private List<TextBlock> tblocks;
        private List<TextBox> tboxes;

        private readonly CultureInfo uiLoc;

        private readonly User doc;

        private readonly MethodicResultService _resultProvider = new MethodicResultService(UserRole.Patient);

        private TestPass(User doc)
        {
            this.doc = doc;
            InitializeComponent();
            QuestionPresenter.ItemPressed += QuestionSelected;
            uiLoc = Thread.CurrentThread.CurrentUICulture;
        }

        private void QuestionSelected(object sender, string e)
        {
            var i = int.Parse(e) - 1;
            if (answers[i].Value.Length == 0)
                viewQuestion(i);
        }

        private void viewQuestion(int index)
        {
            cindex = index;
            lvQuestions.SelectedIndex = cindex;
            lvQuestions.ScrollIntoView(lvQuestions.SelectedItem);
            var q = current.Questions[index];
            tbQuestion.Text = $"{q.Order}. {string.Join(Environment.NewLine, q.Text.Split('^'))}";
            var hd = headers.FirstOrDefault(x => x.Key.Contains(index.ToString()) || x.Key.Intersect(q.Tags).Any());
            tbHeader.Text = hd.Value ?? "";
            tblocks = this.GetChildren<TextBlock>().ToList();
            tboxes = this.GetChildren<TextBox>().ToList();
            imgPic.Source = q.Picture?.AsImage();
            if (current.AnswersType != AnswersType.Variants) return;
            spVariants.Children.Clear();
            var qq = 0;
            foreach (var i in q.Answers)
            {
                var bt = i.UiRepresentation(answerClick);
                bt.Tag = qq++;
                // TODO : listview with grid presenter
                //if (q.PreferredLayout == PreferredLayout.Stack)
                spVariants.Children.Add(bt);
            }
        }
        
        public TestPass(User doctor, Patient p, Methodic m) : this(doctor)
        {
            queue.Enqueue(m);
            patient = p;
            prepareUi();
        }

        public TestPass(User doctor, Patient p, MethodicPack mp) : this(doctor)
        {
            queue = new Queue<Methodic>(mp.Queue);
            patient = p;
            prepareUi();
        }

        private void prepareUi()
        {
            if (queue.Count == 0)
            {
                Thread.CurrentThread.CurrentUICulture = uiLoc;
                MessageBox.Show(Properties.Resources.msg_TestPass_TestFinished);
                new Login().Show();
                Close();
                return;
            }

            current = queue.Dequeue();
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(current.Locale);
            FillHeaders();
            current.Questions.Sort((l, r) => l.Order.CompareTo(r.Order));
            answers = Enumerable.Range(0, current.Questions.Count).ToDictionary(x => x, x => new ResultAnswer());
            var pos = 1;
            foreach (var unused in current.Questions)
                lvQuestions.Items.Add(new QuestionPresenter(pos++.ToString()));
            if (!string.IsNullOrWhiteSpace(current.Instructions))
                wbInstructions.NavigateToString(current.Instructions);
            switch (current.AnswersType)
            {
                case AnswersType.YesNo:
                    spVariants.Orientation = Orientation.Horizontal;
                    var btn = generate(Properties.Resources.const_TestPass_positiveAnswer, 1, answerClick);
                    spVariants.Children.Add(btn);
                    btn = generate(Properties.Resources.const_TestPass_negativeAnswer, 0, answerClick);
                    spVariants.Children.Add(btn);
                    break;
                case AnswersType.Scale:
                    spVariants.Orientation = Orientation.Horizontal;
                    foreach (var i in Enumerable.Range(current.ScaleBase, (int)current.ScaleWidth))
                    {
                        var bt = generate(i.ToString(), i, answerClick);
                        spVariants.Children.Add(bt);
                    }
                    break;
                default:
                    Debug.WriteLine(Properties.Resources.error_TestPass_wrongTestType);
                    break;
            }
            lvQuestions.SelectedIndex = 0;
            viewQuestion(0);
            grLoading.Visibility = Visibility.Collapsed;
        }

        private void FillHeaders()
        {
            headers = current.Headers.Select(x => x.Split("::")).ToDictionary(
                x => x[0].Trim().Split(',').Select(y => y.Trim()).ToArray(),
                x => x[1].Trim().Replace("$", Environment.NewLine));
        }

        private void answerClick(object sender, EventArgs e)
        {
            (lvQuestions.Items[cindex] as QuestionPresenter)?.SetState(true);
            var qorder = current.Questions[cindex].Order;
            var tg = (sender as Control)?.Tag;
            if (tg is null) return;
            switch (tg)
            {
                case byte[] ba when e is ResultEventArgs rea:
                    answers[cindex] = new ResultAnswer
                    {
                        Value = ba,
                        Question = qorder,
                        ResultType = rea.Type
                    };
                    break;
                case int i:
                    answers[cindex] = new ResultAnswer
                    {
                        Value = BitConverter.GetBytes(i),
                        Question = qorder
                    };
                    break;
            }
            if (answers.Values.Any(x => x.Value.Length == 0))
            {
                if (answers.ContainsKey(cindex + 1) && answers[cindex + 1].Value.Length == 0)
                    viewQuestion(cindex + 1);
                else
                {
                    var na = answers.Where(x => x.Value.Value.Length == 0).ToList();
                    if (na.Any(x => x.Key > cindex))
                        viewQuestion(na.Where(x => x.Key > cindex).Min(x => x.Key));
                    viewQuestion(na.Min(x => x.Key)); 
                }
            }
            else
            {
                grLoading.Visibility = Visibility.Visible;
                spVariants.Children.Clear();
                tbHeader.Text = "";
                tbQuestion.Text = "";
                lvQuestions.Items.Clear();
                wbInstructions.NavigateToString("<html></html>");
                Task.Run(() =>
                {
                    var it = new MethodicResult
                    {
                        MethodicName = current.Title,
                        Answers = new List<ResultAnswer>(answers.Values),
                        Doctor = doc,
                        Patient = patient
                    };
                    _resultProvider.Insert(it);
                }).ContinueWith(x => Dispatcher.Invoke(() =>
                {
                    grLoading.Visibility = Visibility.Collapsed;
                    prepareUi();
                }));
            }
        }

        private Button generate(string content, int tag, RoutedEventHandler clickHandler)
        {
            var button = new Button
            {
                VerticalAlignment = VerticalAlignment.Center,
                Content = new TextBlock {Text = content},
                Margin = new Thickness(8),
                Padding = new Thickness(8),
                Tag = tag
            };
            button.Click += clickHandler;
            return button;
        }

        private void TestPass_OnClosing(object sender, CancelEventArgs e)
        {
            if (queue.Count > 0 || answers.Values.Any(x => x.Value.Length == 0))
                e.Cancel = true;
        }



        private void clickScaleUp(object sender, RoutedEventArgs e)
        {
            if (!(tblocks is null))
                foreach (var a in tblocks)
                    a.FontSize = Math.Min(40, a.FontSize + 2);
            if (!(tboxes is null))
                foreach (var a in tboxes)
                    a.FontSize = Math.Min(40, a.FontSize + 2);
            lvQuestions.Items.Refresh();
        }

        private void clickScaleDown(object sender, RoutedEventArgs e)
        {
            if (!(tblocks is null))
                foreach (var a in tblocks)
                    a.FontSize = Math.Max(2, a.FontSize - 2);
            if (!(tboxes is null))
                foreach (var a in tboxes)
                    a.FontSize = Math.Max(2, a.FontSize - 2);
            lvQuestions.Items.Refresh();
        }
    }
}
