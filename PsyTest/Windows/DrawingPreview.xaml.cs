﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для DrawingPreview.xaml
    /// </summary>
    public partial class DrawingPreview : Window
    {
        public DrawingPreview()
        {
            InitializeComponent();
        }

        private WriteableBitmap bitmap;
        public bool error;

        public DrawingPreview(string question, byte[] drawing) : this()
        {
            Title = question;
            var wid = BitConverter.ToInt16(drawing, 0);
            var hei = BitConverter.ToInt16(drawing, 2);
            if (drawing.Length == wid * hei * 4 + 4)
            {
                var dr = drawing.Skip(4).ToArray();
                bitmap = new WriteableBitmap(wid, hei, 96,96, PixelFormats.Pbgra32, null);
                imgPreview.Source = bitmap;
                bitmap.WritePixels(new Int32Rect(0, 0, wid, hei), dr, wid* 4, 0);
            }
            else
            {
                MessageBox.Show(Properties.Resources.error_DrawingPreview_corruptedDrawing);
                error = true;
            }
        }
    }
}
