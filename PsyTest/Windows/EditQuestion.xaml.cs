﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using meet.app.netcore.Extensions;
using meet.models.Methodical;
using meet.wpf.controls.Windows;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для EditQuestion.xaml
    /// </summary>
    public partial class EditQuestion : Window, INotifyPropertyChanged
    {
        public Question Source { get; set; } = new Question();
        public Question Binder { get; set; }

        public string PicPath
        {
            get => _picPath;
            set
            {
                if (value == _picPath) return;
                _picPath = value;
                OnPropertyChanged();
            }
        }

        private bool exitState;
        private string _picPath;

        public EditQuestion()
        {
            Binder = new Question();
            InitializeComponent();
            lpTags.CreateProvider = () =>
                InputBox.ShowDialog(Properties.Resources.title_EditQuestion_inputTags)?
                    .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim());
            lpAnswers.CreateProvider = () => new Answer();
            lpTags.ItemsSource = Binder.Tags;
            lpAnswers.ItemsSource = Binder.Answers;
        }

        public EditQuestion(Question q) : this()
        {
            Source = q;
            Binder.Load(Source);
        }

        private void clickSave(object sender, RoutedEventArgs e)
        {
            exitState = true;
            Close();
        }

        private void cancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void EditQuestion_OnClosing(object sender, CancelEventArgs e)
        {
            if (exitState)
                Source.Load(Binder);
        }

        private void doubleClickAnswer(object sender, MouseButtonEventArgs e)
        {
            new EditAnswer(lpAnswers.SelectedItem as Answer).ShowDialog();
        }

        private void doubleClickTag(object sender, MouseButtonEventArgs e)
        {
            Binder.Tags[lpTags.SelectedIndex] = InputBox.ShowDialog(Properties.Resources.title_EditQuestion_editTag, lpTags.SelectedItem as string);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void clickLoadPic(object sender, RoutedEventArgs e)
        {
            var ofd = "Select picture".AsOpenFileDialog(Properties.Resources.filter_EditAnswer_bitmapPics);
            if (ofd.ShowDialog() != true)
                return;
            Binder.Picture = File.ReadAllBytes(ofd.FileName);
        }
    }
}
