﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using meet.app.netcore.Extensions;
using meet.common;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.Methodical.Representational;
using meet.models.PassResults;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для BatchExport.xaml
    /// </summary>
    public partial class BatchExport
    {
        public class ResultProxy : INotifyPropertyChanged
        {
            private double _maxValue;
            private double _value;
            private bool _indeterminate;

            private readonly MethodicsService _provider = new MethodicsService(Login.Role);

            public ResultProxy(MethodicResult methodicResult)
            {
                Result = methodicResult;
                string name = methodicResult.MethodicName;
                Methodic = _provider.GetItems().SingleOrDefault(y => y.Title == name);
                Indeterminate = true;
                Value = 0;
                MaxValue = Methodic.Representation.Representations.Count(x =>
                    x is GraphicalRepresentation || (x is PlainRepresentation pr && pr.IsVisible));
                Title = Properties.Resources.ui_ResultProxy_waiting;
            }

            public MethodicResult Result { get; set; }
            public Methodic Methodic { get; set; }
            public string Title { get; set; }
            public double MaxValue {
                get => _maxValue;
                set {
                    if (value.Equals(_maxValue)) return;
                    _maxValue = value;
                    OnPropertyChanged();
                }
            }

            public double Value {
                get => _value;
                set {
                    if (value.Equals(_value)) return;
                    _value = value;
                    OnPropertyChanged();
                }
            }

            public bool Indeterminate {
                get => _indeterminate;
                set {
                    if (value == _indeterminate) return;
                    _indeterminate = value;
                    OnPropertyChanged();
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }

            public void StateChange(bool? ind, string cap, int? val)
            {
                if (ind.HasValue)
                    Indeterminate = ind.Value;
                if (!string.IsNullOrWhiteSpace(cap))
                    Title = cap;
                if (val.HasValue)
                    Value += val.Value;
            }
        }

        private bool inprog;

        public List<ResultProxy> Source { get; set; } = new List<ResultProxy>();
        private ConcurrentBag<string> paths = new ConcurrentBag<string>();
        private readonly MethodicResultService _resultService = new MethodicResultService(Login.Role);

        public BatchExport()
        {
            InitializeComponent();
            inprog = true;
        }

        public BatchExport(IEnumerable<MethodicResult> results, bool exportRaw) : this()
        {
            Task.Run(() =>
            {
                foreach (var result in results)
                {
                    Source.Add(new ResultProxy(result));
                    Dispatcher?.Invoke(() => lvItems.Items.Refresh());
                }
            }).ContinueWith(x => Dispatcher.Invoke(() =>
                {
                    lvItems.Items.Refresh();
                    grLoading.Visibility = Visibility.Collapsed;
                }))
                .ContinueWith(_ =>
             Task.WhenAll(lvItems.Items.OfType<ResultProxy>().Select(x =>
             {
                 return Task.Factory.StartNew(i =>
                 {
                     var t = i as ResultProxy;
                     t.Indeterminate = false;
                     t.Title = Properties.Resources.ui_ResultProxy_exporting;
                     paths.Add(t.Result.ExportToWord(t.Methodic, t.StateChange, _resultService,  true,   exportRaw, Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)));
                 }, x);
             })).ContinueWith(x =>
             {
                 inprog = false;
                 Dispatcher?.Invoke(Close);
             }));
        }

        private void BatchExport_OnClosing(object sender, CancelEventArgs e)
        {
            if (inprog)
            {
                e.Cancel = true;
                return;
            }
            paths.Distinct().ToList().ForEach(Extensions.Extensions.OpenExplorer);
        }
    }
}
