﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.PassResults;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для SelectPatient.xaml
    /// </summary>
    public partial class SelectPatient : Window
    {
        private readonly PatientsProvider _patientsProvider = new PatientsProvider(Login.Role);
        private readonly MethodicsService _methodicProvdier = new MethodicsService(Login.Role);

        public SelectPatient()
        {
            InitializeComponent();
            Task.Run(() =>
            {
                var pts = _patientsProvider.GetItems();
                var mts = _methodicProvdier.GetItems().ToList();

                Dispatcher.Invoke(() =>
                {
                    cbPatients.ItemsSource = pts;
                    cbMethodics.ItemsSource = mts;
                    grLoading.Visibility = Visibility.Collapsed;
                });
            });
        }

        private void clickOk(object sender, RoutedEventArgs e)
        {
            if (!(cbPatients.SelectedItem is Patient sp)) {
                MessageBox.Show(Properties.Resources.error_SelectPatient_blankPatient);
                return;
            }
            switch (cbMethodics.SelectedItem) {
                case Methodic sm:
                    new TestPass(Login.CurrentUser, sp, sm).Show();
                    break;
                case MethodicPack mp:
                    new TestPass(Login.CurrentUser, sp, mp).Show();
                    break;
                case null:
                    MessageBox.Show(Properties.Resources.error_SelectPatient_blankMethodic);
                    break;
                default:
                    MessageBox.Show(Properties.Resources.error_SelectPatient_unexpected);
                    return;
            }
            Close();
        }

        private void clickAddPatient(object sender, RoutedEventArgs e)
        {
            var src = CreatePatient.ShowDialog(true);
            if (src is null) return;
            _patientsProvider.Insert(src);
            cbPatients.ItemsSource = _patientsProvider.GetItems();

        }

        private void clickCancel(object sender, RoutedEventArgs e)
        {
            new Panel().Show();
            Close();
        }
    }
}
