﻿using System.ComponentModel;
using System.Windows;
using meet.common;
using meet.models.Auth;
using meet.models.PassResults;

namespace meet.app.netcore.Windows {
    /// <summary>
    /// Логика взаимодействия для CreatePatient.xaml
    /// </summary>
    public partial class CreatePatient : Window
    {
        public Patient Patient { get; set; } = new Patient{User = new User{Role = UserRole.Patient}};
        private bool isOk;
        private CreatePatient() {
            InitializeComponent();
        }

        public static Patient ShowDialog(bool unused) {
            var inst = new CreatePatient();
            inst.ShowDialog();
            return inst.Patient;
        }

        private void clickSave(object sender, RoutedEventArgs e) {
            Patient.User.Identifier = tbCode.Text;
            isOk = true;
            Close();
        }

        private void clickCancel(object sender, RoutedEventArgs e) => Close();

        private void OnClosing(object sender, CancelEventArgs e)
        {
            if (!isOk) Patient = null;
        }
    }
}
