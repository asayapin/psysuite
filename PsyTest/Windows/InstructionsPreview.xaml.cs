﻿using System.Collections.Concurrent;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using meet.models.Methodical;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для InstructionsPreview.xaml
    /// </summary>
    public partial class InstructionsPreview : Window
    {
        private readonly Methodic Src;
        private readonly ConcurrentStack<int> stack = new ConcurrentStack<int>();

        public InstructionsPreview()
        {
            InitializeComponent();
        }

        public InstructionsPreview(Methodic m) : this()
        {
            Src = m;
            SourceCode = m.Instructions;
        }

        private void InstructionsPreview_OnClosing(object sender, CancelEventArgs e)
        {
            if (Src is null) return;
            Src.Instructions = SourceCode;
        }

        private string SourceCode
        {
            get => new TextRange(rtbSource.Document.ContentStart, rtbSource.Document.ContentEnd).Text;
            set => new TextRange(rtbSource.Document.ContentStart, rtbSource.Document.ContentEnd).Text = value ?? "";
        }

        private void textChangeSource(object sender, TextChangedEventArgs e)
        {
            stack.Push(0);
            Task.Delay(1500).ContinueWith(x =>
            {
                if (!stack.TryPop(out var _)) return;
                if (stack.IsEmpty)
                    Dispatcher.Invoke(() => wbPreview.NavigateToString(string.IsNullOrWhiteSpace(SourceCode) ? "<html></html>" : SourceCode));
            });
        }
    }
}
