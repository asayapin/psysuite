﻿using System.IO;
using System.Linq;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для About.xaml
    /// </summary>
    public partial class About
    {
        private string ReadResource(string name)
        {
            var asm = typeof(About).Assembly;
            var nm = asm.GetManifestResourceNames().FirstOrDefault(x => x.Contains(name));
            if (!string.IsNullOrWhiteSpace(nm))
            {
                var str = asm.GetManifestResourceStream(nm);
                if (str is null) return null;
                using (var sr = new StreamReader(str, true))
                    return sr.ReadToEnd();
            }
            return null;
        }

        public About()
        {
            InitializeComponent();

            wbAdmin.NavigateToString(ReadResource("Admin") ?? "<html></html>");
            wbUser.NavigateToString(ReadResource("User") ?? "<html></html>");
            wbCreds.NavigateToString(ReadResource("About") ?? "<html></html>");
        }
    }
}
