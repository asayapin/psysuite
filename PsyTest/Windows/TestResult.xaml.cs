﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using meet.app.netcore.Extensions;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.PassResults;
// ReSharper disable PossiblyMistakenUseOfParamsMethod

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для TestResult.xaml
    /// </summary>
    public partial class TestResult
    {
        public class RawResult
        {
            public string Question { get; set; }
            public string Answer { get; set; }
            public RawResult(string q, string a)
            {
                Question = q;
                Answer = a;
            }
        }

        public bool err;
        private bool exporting;
        private MethodicResult preview;
        private Methodic meth;

        private readonly MethodicsService _methodicProvdier = new MethodicsService(Login.Role);
        private readonly MethodicResultService _resultProvider = new MethodicResultService(Login.Role);

        public TestResult() => InitializeComponent();

        public TestResult(MethodicResult rp) : this()
        {
            if (rp is null)
            {
                MessageBox.Show(Properties.Resources.error_TestResult_sourceIsNull);
                Close();
                err = true;
                return;
            }

            Task.Run(() =>
            {
                Dispatcher?.Invoke(() =>
                {
                    tbPatient.Text = rp.Patient.ToString();
                    tbMethodic.Text = rp.MethodicName;
                });
                var answers = new List<ResultAnswer>();

                meth = _methodicProvdier.GetItems().SingleOrDefault(x => x.Title == rp.MethodicName);
                if (meth is null)
                {
                    Dispatcher?.Invoke(() =>
                    {
                        MessageBox.Show(Properties.Resources.error_TestResult_wrongMethodicInResult);
                        Close();
                    });
                    err = true;
                    return;
                }

                preview = rp;
                Dispatcher?.Invoke(() =>
                tbTimestamp.Text = new DateTimeOffset(preview.PassTimestamp, TimeSpan.FromMinutes(preview.UtcOffset))
                    .ToString());
                answers.AddRange(rp.Answers);

                var rr = meth.Questions.Zip(answers, meth.MakeRawResult).ToList();
                Dispatcher?.Invoke(() =>
                {
                    dgRawResults.ItemsSource = rr;

                    foreach (UIElement el in meth.Representation.Build(meth, preview, _resultProvider))
                    {
                        if (!(el is null))
                            spRepresentations.Children.Add(el);
                    }

                    grLoading.Visibility = Visibility.Collapsed;
                });
            });
        }

        private void doubleClickAnswer(object sender, MouseButtonEventArgs e)
        {
            var s = (dgRawResults.SelectedItem as RawResult)?.Answer;
            if (s == Properties.Resources.itemConst_drawingAnswer)
            {
                var q = meth.Questions[preview.Answers[dgRawResults.SelectedIndex].Question].Text;
                var dr = preview.Answers[dgRawResults.SelectedIndex].Value;
                var dp = new DrawingPreview(q, dr);
                dp.Show();
                if (dp.error)
                    dp.Close();
            }
        }

        private void DgRawResults_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.Column.Header.ToString())
            {
                case nameof(RawResult.Question):
                    e.Column.Header = Properties.Resources.header_TestResult_question;
                    break;
                case nameof(RawResult.Answer):
                    e.Column.Header = Properties.Resources.header_TestResult_answer;
                    break;
            }
        }

        private void clickExport(object sender, RoutedEventArgs e)
        {
            if (exporting) return;
            exporting = true;
            pbExport.Visibility = Visibility.Visible;
            tbExportText.Text = Properties.Resources.header_TestResult_processing;
            Task.Run(() => preview.ExportToWord(meth, null, _resultProvider, false, false)).ContinueWith(x =>
            {
                Dispatcher?.Invoke(() =>
                {
                    pbExport.Visibility = Visibility.Collapsed;
                    tbExportText.Text = Properties.Resources.ui_TestResults_export;
                    exporting = false;
                });
            });
        }
    }
}
