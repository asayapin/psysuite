﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.PassResults;
using meet.wpf.controls.Windows;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для MethodicsPreview.xaml
    /// </summary>
    public partial class MethodicsPreview : Window
    {
        public class MethodicProxy
        {
            public string Code, Title;
            internal Methodic Methodic;
            internal static bool ShowCode = true, ShowTitle;
            public string Locale { get; set; }

            public override string ToString() => $"{(ShowCode ? Code : "")} {(ShowTitle ? Title : "")}";
        }

        private List<MethodicProxy> proxies;
        private bool locker;

        private readonly MethodicsService _methodicsProvider = new MethodicsService(Login.Role);
        private readonly AbstractProvider<Patient> _patientsProvider = new PatientsProvider(Login.Role);

        public MethodicsPreview()
        {
            InitializeComponent();
            Task.Run(() => {
                proxies = _methodicsProvider.GetItems().Where(x => !(x is null))
                    .Select(x => new MethodicProxy { Code = x.Code, Title = x.Title, Locale = x.Locale, Methodic = x }).ToList();
                Dispatcher.Invoke(() => rebuildList(true));
            }).ContinueWith(x => Dispatcher.Invoke(() => grLoading.Visibility = Visibility.Collapsed));
            cbCode.IsChecked = true;
        }

        private void rebuildList(bool full = false)
        {
            if (full) {
                lvMethodics.Items.Clear();
                foreach (var mp in proxies) {
                    var box = new CheckBox { Content = mp, Tag = mp };
                    box.Checked += itemChecked;
                    box.Unchecked += itemChecked;
                    lvMethodics.Items.Add(box);
                }

                return;
            }
            foreach (var box in lvMethodics.Items.OfType<CheckBox>())
                box.Content = box.Tag.ToString();
        }

        private void itemChecked(object sender, RoutedEventArgs e)
        {
            var boxs = lvMethodics.Items.OfType<CheckBox>().Select(x => x.IsChecked).ToList();
            if (!locker)
                cbSelector.IsChecked = boxs.All(x => x == true) ? true : boxs.All(x => x == false) ? false : (bool?)null;
        }

        private void clickCancel(object sender, RoutedEventArgs e)
        {
            new Panel().Show();
            Close();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            MethodicProxy.ShowCode = cbCode.IsChecked == true;
            MethodicProxy.ShowTitle = cbTitle.IsChecked == true;
            rebuildList();
        }

        private void clickStart(object sender, RoutedEventArgs e)
        {
            var sel = lvMethodics.Items.OfType<CheckBox>().Where(x => x.IsChecked == true).Select(x => x.Tag).OfType<MethodicProxy>().Select(x => x.Methodic).ToList();
            var mp = MethodicPack.FromEnumerable(sel);

            if (!(InputBox.ShowDialog(Properties.Resources.msg_MethodicsPreview_selectPatient, _patientsProvider.GetItems()) is Patient p))
                return;
            new TestPass(Login.CurrentUser, p, mp).Show();

            Close();
        }

        private void toggleSelector(object sender, RoutedEventArgs e)
        {
            if (locker) return;
            locker = true;
            if (cbSelector.IsChecked != null)
                foreach (var box in lvMethodics.Items.OfType<CheckBox>())
                    box.IsChecked = cbSelector.IsChecked;
            locker = false;
        }

        private void clickUp(object sender, RoutedEventArgs e) {
            var ind = lvMethodics.SelectedIndex;
            if (ind == 0) return;
            var tmp = lvMethodics.SelectedItem;
            lvMethodics.Items.RemoveAt(lvMethodics.SelectedIndex);
            lvMethodics.Items.Insert(ind - 1, tmp);
            lvMethodics.SelectedIndex = ind - 1;
        }

        private void clickDown(object sender, RoutedEventArgs e) {
            var ind = lvMethodics.SelectedIndex;
            if (ind == lvMethodics.Items.Count - 1) return;
            var tmp = lvMethodics.SelectedItem;
            lvMethodics.Items.RemoveAt(lvMethodics.SelectedIndex);
            lvMethodics.Items.Insert(ind + 1, tmp);
            lvMethodics.SelectedIndex = ind + 1;
        }

        private void toggleLocale(object sender, RoutedEventArgs e)
        {
            foreach (var box in lvMethodics.Items.OfType<CheckBox>())
            {
                if (!(box.Tag is MethodicProxy mt)) continue;
                box.Visibility =
                    (mt.Locale == "en" && cbEn.IsChecked == true) || (mt.Locale == "ru" && cbRu.IsChecked == true)
                        ? Visibility.Visible
                        : Visibility.Collapsed;
            }
            lvMethodics.Items.Refresh();
        }
    }
}
