﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using meet.app.netcore.Extensions;
using meet.models;
using meet.models.Db;
using meet.models.Methodical;
using meet.models.Methodical.Representational;
using meet.wpf.controls.Windows;
using MatchType = meet.models.MatchType;

namespace meet.app.netcore.Windows
{
    /// <summary>
    /// Логика взаимодействия для EditTest.xaml
    /// </summary>
    public partial class EditTest 
    {
        public Methodic Source { get; set; }
        private readonly AnswersType[] answersTypes = new[] { AnswersType.Scale, AnswersType.Variants, AnswersType.YesNo };
        private readonly MethodicsService _provider = new MethodicsService(Login.Role);

        public EditTest()
        {
            Source = new Methodic();
            InitializeComponent();


            cbAType.ItemsSource = answersTypes;
            cbAType.SelectedIndex = 0;

            lpQuestions.CreateProvider = () => new Question();
            lpQuestions.CustomButtons = new Dictionary<string, RoutedEventHandler> {
                [Properties.Resources.header_EditTest_browseLP] = (sender, args) => {
                    var ofd = Properties.Resources.title_EditTest_questionListOFD.AsOpenFileDialog(Properties.Resources.filter_anyFile);
                    if (ofd.ShowDialog() != true) return;
                    var ctn = File.ReadAllText(ofd.FileName);
                    var qs = ctn.Split(new[] { '$' }, StringSplitOptions.RemoveEmptyEntries);
                    Source.Questions.AddRange(qs.Select((x, i) => FromText(i + 1, x, Source.AnswersType)));
                    lpQuestions.RefreshView();
                },
                [Properties.Resources.uiConst_up] = (sender, args) => {
                    lpQuestions.SelectedIndex = Source.Questions.Up(lpQuestions.SelectedIndex);
                    lpQuestions.RefreshView();
                },
                [Properties.Resources.uiConst_down] = (sender, args) => {
                    lpQuestions.SelectedIndex = Source.Questions.Down(lpQuestions.SelectedIndex);
                    lpQuestions.RefreshView();
                }
            };
            lpQuestions.ItemsSource = Source.Questions;

            lpHeaders.CustomButtons = new Dictionary<string, RoutedEventHandler> {
                [Properties.Resources.uiConst_browse] = (sender, args) => {
                    var ofd = Properties.Resources.title_EditTest_headerRulesOFD.AsOpenFileDialog(Properties.Resources.filter_anyFile);
                    if (ofd.ShowDialog() != true) return;
                    var ctn = File.ReadAllLines(ofd.FileName);
                    Source.Headers.AddRange(ctn.Where(x => x.Contains("::")));
                    lpHeaders.RefreshView();
                },
            };
            lpHeaders.CreateProvider = () => InputBox.ShowDialog(Properties.Resources.title_EditTest_headerRuleIBox);
            lpHeaders.ItemsSource = Source.Headers;
            Dispatcher.InvokeAsync(() => {
                LoadMethodics();
                grLoading.Visibility = Visibility.Collapsed;
            });
        }

        private void LoadMethodics(string target = null)
        {
            var mts = new List<Methodic> { new Methodic { Title = Properties.Resources.headerConst_createItemCB }, new Methodic { Title = Properties.Resources.header_EditTest_loadFromTextCB } };
            mts.AddRange(_provider.GetItems() ?? new List<Methodic>());
            var ci = mts.FindIndex(x => x.Title == target);
            cbMethodic.ItemsSource = mts;
            cbMethodic.SelectedIndex = ci;
        }

        private Question FromText(int i, string src, AnswersType type)
        {
            var q = new Question{Order = i};
            src = src.Trim();

            var ss = src.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (ss[0].StartsWith("[")) {
                switch (ss[0].Substring(1, ss[0].IndexOf(']') - 1).Trim()) {
                    case "s":
                        q.PreferredLayout = PreferredLayout.Stack;
                        break;
                    case "g":
                        q.PreferredLayout = PreferredLayout.Grid;
                        break;
                }

                ss[0] = ss[0].Substring(ss[0].IndexOf(']') + 1).Trim();
            }

            q.Text = ss[0].Replace("^", Environment.NewLine);
            var wpic = false;
            if (ss.ElementAtOrDefault(1)?.StartsWith("{") == true)
            {
                q.Picture = File.ReadAllBytes(ss[1].Trim('{', '}'));
                wpic = true;
            }

            if (type != AnswersType.Variants) return q;
            foreach (var item in ss.Skip(wpic ? 2 : 1)) {
                if (item.StartsWith("[")) {
                    var typ = item.Substring(1, item.IndexOf(']') - 1);
                    switch (typ) {
                        case "p":
                            var pt = item.Substring(item.IndexOf(']') + 1).Trim();
                            if (File.Exists(pt))
                                q.Answers.Add(new Answer {
                                    AnswerType = AnswerType.Picture,
                                    Data = File.ReadAllBytes(pt)
                                });
                            break;
                        case "f":
                            q.Answers.Add(new Answer { AnswerType = AnswerType.FreeText }); break;
                        case "d":
                            q.Answers.Add(new Answer { AnswerType = AnswerType.Drawing });
                            break;
                    }
                } else q.Answers.Add(new Answer { AnswerType = AnswerType.Text, Data = Encoding.UTF8.GetBytes(item) });
            }
            return q;
        }

        private void selectedMethodic(object sender, RoutedEventArgs e)
        {
            if (!(cbMethodic.SelectedItem is Methodic m)) {
                Debug.WriteLine(cbMethodic.SelectedItem, "selected item is not methodic");
                return;
            }

            if (m.Title == Properties.Resources.headerConst_createItemCB)
            {
                Source.Load(new Methodic());
            }
            else if (m.Title == Properties.Resources.header_EditTest_loadFromTextCB)
            {
                var ofd = Properties.Resources.title_EditTest_methodicTextOFD.AsOpenFileDialog(Properties.Resources.filter_anyFile);
                if (ofd.ShowDialog() == true)
                {
                    Source.Load(loadFromText(ofd.FileName));
                }
            }
            else
            {
                Source.Load(m);
            }

            lpQuestions.RefreshView();
            cbAType.SelectedIndex = answersTypes.ToList().IndexOf(Source.AnswersType);
            lpHeaders.RefreshView();
        }

        private Methodic loadFromText(string path)
        {
            if (path is null || !File.Exists(path)) return null;
            var mth = new Methodic();
            var inst = Directory.EnumerateFiles(Path.GetDirectoryName(path), $"{Path.GetFileName(Path.ChangeExtension(path, "htm"))}*").FirstOrDefault();
            if (!(inst is null))
                mth.Instructions = File.ReadAllText(inst);
            mth.Title = Path.GetFileNameWithoutExtension(path);

            var text = File.ReadAllText(path).Split('&');

            mth.Headers.AddRange(text[0].Split(new[]{'\r','\n'}, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()));

            var mask = text[1].TrimEnd('+', '-').Trim().ToLower();

            switch (mask) {
                case "y":
                    mth.AnswersType = AnswersType.YesNo;
                    break;
                case "v":
                    mth.AnswersType = AnswersType.Variants;
                    break;
                case { } when mask.Count(x => x == '.') == 1:
                    var pts = mask.Split('.').Select(x => x.Trim()).ToArray();
                    if (!pts.All(x => uint.TryParse(x, out _))) break;
                    mth.AnswersType = AnswersType.Scale;
                    mth.ScaleWidth = uint.Parse(pts[0]);
                    mth.ScaleBase = int.Parse(pts[1]);
                    break;
            }

            mth.Questions.AddRange(text[2].Trim().Split(new[] { '$' }, StringSplitOptions.RemoveEmptyEntries).Select((x, i) => FromText(i + 1, x, mth.AnswersType)));

            var lns = text[3].Trim().Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var ln in lns) {
                var spl = ln.Split(new[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                var numbers = spl[1].Trim().Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);
                var tag = spl[0].Trim();
                foreach (var a in numbers) {
                    mth.Questions.FirstOrDefault(x => x.Order == a)?.Tags.Add(tag);
                }
            }

            mth.Representation = loadRepresentations(text.ElementAtOrDefault(4));

            return mth;
        }

        private Representation loadRepresentations(string src)
        {
            var res = new Representation();
            if (string.IsNullOrWhiteSpace(src)) return res;

            var rps = src.Split(new[] { '$' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).Select(loadSub).ToList();

            var prs = rps.OfType<PlainRepresentation>().ToList();
            var pnms = prs.Select(x => x.Name);
            
            res.Representations.AddRange(prs.Concat(rps.OfType<GraphicalRepresentation>().Where(x => x.Values.All(pnms.Contains)).Cast<SubRepresentation>()));

            return res;
        }

        private SubRepresentation loadSub(string src)
        {
            var data = src.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();
            switch (src[0]) {
                case 'P':
                    var pr = new PlainRepresentation();
                    pr.IsVisible = src[1] == '+';
                    switch (src[2])
                    {
                        case 'G': pr.MatchType = MatchType.Greater; break;
                        case 'L': pr.MatchType = MatchType.Less; break;
                        case 'V': pr.MatchType = MatchType.ByValue; break;
                        case 'E': pr.MatchType = MatchType.Equal; break;
                    }

                    foreach (var a in data.Skip(1))
                    {
                        switch (a[0]) {
                            case '[':
                                pr.Pattern = a.Trim('[',']');
                                break;
                            case '{':
                                var dt = a.Trim('{', '}').Split(',').Select(x => x.Trim());
                                pr.Tags.AddRange(dt);
                                break;
                            case '<':
                                var dq = a.Trim('<', '>').Split(',').Select(x => x.Trim()).Select(x => int.TryParse(x, out var i) ? i : -1);
                                pr.Questions.AddRange(dq); break;
                            default:
                                pr.PostProcess = a;
                                break;
                        }
                    }

                    pr.Name = data[0].Substring(data[0].IndexOf(' ') + 1);
                    return pr;
                case 'G':
                    var rt = new GraphicalRepresentation();
                    var ds = data[0].Split(' ');
                    if (Enum.TryParse(ds.ElementAtOrDefault(2), out SeriesChartType sct))
                        rt.GraphType = (int)sct;
                    rt.Values.AddRange(data.Skip(3));
                    rt.Name = ds.ElementAtOrDefault(1);
                    return rt;
            }
            return null;
        }

        private void clickClearInstructions(object sender, RoutedEventArgs e)
        {
            Source.Instructions = string.Empty;
        }

        private void clickPreviewInstructions(object sender, RoutedEventArgs e)
        {
            new InstructionsPreview(Source).ShowDialog();
        }

        private void clickBrowseInstructions(object sender, RoutedEventArgs e)
        {
            var ofd = Properties.Resources.title_EditTest_instructionsOFD.AsOpenFileDialog(Properties.Resources.filter_EditTest_webpages);
            if (ofd.ShowDialog() == true) {
                tbInstPath.Text = ofd.FileName;
                // TODO : file system listener + messagebox for reload
                try {
                    Source.Instructions = File.ReadAllText(ofd.FileName);
                } catch (Exception ex) {
                    MessageBox.Show(string.Format(Properties.Resources.error_EditTest_generic, ex.Message));
                }
            }
        }

        private void selectedType(object sender, SelectionChangedEventArgs e)
        {
            if (!(cbAType.SelectedItem is AnswersType at)) return;
            Source.AnswersType = at;
            grWidth.Visibility = at == AnswersType.Scale ? Visibility.Visible : Visibility.Collapsed;
        }

        private void doubleClickQuestion(object sender, MouseButtonEventArgs e)
        {
            if (!(lpQuestions.SelectedItem is Question q)) return;
            new EditQuestion(q).ShowDialog();
            lpQuestions.RefreshView();
        }

        private void clickRestoreState(object sender, RoutedEventArgs e)
        {
            Source.Load(cbMethodic.SelectedItem as Methodic);
        }

        private void clickCancel(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void clickSave(object sender, RoutedEventArgs e)
        {
            grLoading.Visibility = Visibility.Visible;
            Dispatcher?.InvokeAsync(() => {
                try
                {
                    _provider.InsertOrUpdate(Source, px => px.Title, (kl, kr) => kl.Equals(kr));
                    LoadMethodics(Source.Title);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                grLoading.Visibility = Visibility.Collapsed;
            });
        }
        
        private void clickReprManage(object sender, RoutedEventArgs e)
        {
            new RepresentationManager(Source).ShowDialog();
        }

        private void doubleClickHeaders(object sender, MouseButtonEventArgs e)
        {
            Source.Headers[lpHeaders.SelectedIndex] = InputBox.ShowDialog(Properties.Resources.title_EditTest_headerRuleIBox, lpHeaders.SelectedItem as string);
            lpHeaders.RefreshView();
        }
    }
}
