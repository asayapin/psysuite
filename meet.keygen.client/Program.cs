﻿using System;
using meet.common;

namespace meet.keygen.client
{
    internal static class Program
    {
        private static void Main()
        {
            while (true)
            {
                var role = UserRole.None;
                var duration = 0U;
                while (role == UserRole.None)
                {
                    if (uint.TryParse(Prompt("role? (1 = admin, 2 = doctor, 3 = patient)"), out var uRole)
                        && uRole <= 3
                        && uRole > 0)
                        role = (UserRole) uRole;
                }

                while (duration == 0)
                {
                    if (uint.TryParse(Prompt("duration? (1..12 hours)", true), out var dur)
                        && dur <= 12
                        && dur > 0)
                        duration = dur;
                }
                Console.WriteLine(role.CreateKey(2, duration));
                if (Prompt("Generate key? ([y]es, [n]o)") != "y") return;
            }
        }

        private static string Prompt(string prompt, bool key = false)
        {
            Console.Write(prompt);
            if (!key)
                Console.WriteLine();
            return key ? Console.ReadLine() : Console.ReadKey(true).KeyChar.ToString();
        }
    }
}
