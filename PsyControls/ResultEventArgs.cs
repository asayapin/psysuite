﻿using System;
using meet.models;

namespace meet.wpf.controls
{
    public class ResultEventArgs : EventArgs
    {
        public readonly ResultType Type;

        public ResultEventArgs(ResultType type) => Type = type;
    }
}
