﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace meet.wpf.controls.Windows
{
    /// <summary>
    /// Логика взаимодействия для MultiSelect.xaml
    /// </summary>
    public partial class MultiSelect
    {
        private List<object> _selected;

        private readonly Dictionary<string, bool> _options;

        public static string Ok { get; set; }
        public static string Cancel { get; set; }
        public static string Loading { get; set; }

        private MultiSelect(string prompt, Dictionary<string, bool> options)
        {
            InitializeComponent();
            _options = options;
            tbPrompt.Text = prompt;
        }
        
        private void Init<T>(IEnumerable<T> src)
        {
            if (Dispatcher is null) return;
            Task.Run(() =>
            {
                var ls = new List<CheckBox>();
                foreach (var a in src)
                {
                    var str = a?.ToString();
                    var cb = Dispatcher.Invoke(() => new CheckBox
                    { Content = str, Margin = new Thickness(4), Tag = a });
                    cb.Checked += delegate
                    {
                        if (lvItems.Items.OfType<CheckBox>().All(x => x.IsChecked == true))
                            cbAll.IsChecked = true;
                        else cbAll.IsChecked = null;
                    };
                    cb.Unchecked += delegate
                    {
                        if (lvItems.Items.OfType<CheckBox>().All(x => x.IsChecked == false))
                            cbAll.IsChecked = false;
                        else cbAll.IsChecked = null;
                    };
                    ls.Add(cb);
                }

                Dispatcher.Invoke(() => lvItems.ItemsSource = ls);
            }).ContinueWith(x => Dispatcher.Invoke(() =>
            {
                grLoading.Visibility = Visibility.Collapsed;
                lvItems.Items.Refresh();
            }));

            if (_options is null) return;
            foreach (var a in _options.Keys)
                spOpts.Children.Add(new CheckBox { Content = a, Tag = a, IsChecked = _options[a] });
        }

        public static IEnumerable<T> ShowDialog<T>(string prompt, IEnumerable<T> source, Dictionary<string, bool> options = null)
        {
            var ms = new MultiSelect(prompt, options);
            ms.Init(source);
            ms.ShowDialog();
            return ms._selected.OfType<T>();
        }

        private void ClickSave(object sender, RoutedEventArgs e)
        {
            _selected = lvItems.Items.OfType<CheckBox>().Where(x => x.IsChecked == true).Select(x => x.Tag).ToList();
            foreach (var box in spOpts.Children.OfType<CheckBox>())
                _options[box.Tag as string ?? ""] = box.IsChecked == true;
            Close();
        }

        private void ClickCancel(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            foreach (CheckBox cb in lvItems.Items)
                cb.IsChecked = true;
        }

        private void ToggleButton_OnUnchecked(object sender, RoutedEventArgs e)
        {
            foreach (CheckBox cb in lvItems.Items)
                cb.IsChecked = false;
        }
    }
}
