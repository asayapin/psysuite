﻿using System.Collections;
using System.Threading.Tasks;
using System.Windows;

namespace meet.wpf.controls.Windows
{
    /// <summary>
    /// Логика взаимодействия для InputBox.xaml
    /// </summary>
    public partial class InputBox
    {
        private bool exitState;
        private readonly bool type;
        public string Result { get; set; }
        public static string Loading { get; set; }
        private object Res;
        public static string Ok { get; set; }
        public static string Cancel { get; set; }
        private InputBox(string caption, string res)
        {
            Result = res;
            InitializeComponent();
            tbHdr.Text = caption;
            grLoading.Visibility = Visibility.Collapsed;
        }

        private InputBox(string caption, IEnumerable src)
        {
            InitializeComponent();
            tbHdr.Text = caption;
            type = true;
            cbAnswer.Visibility = Visibility.Visible;
            tbAnswer.Visibility = Visibility.Collapsed;
            Task.Run(() => Dispatcher?.Invoke(()=>cbAnswer.ItemsSource = src)).ContinueWith(x => Dispatcher?.Invoke(()=>grLoading.Visibility = Visibility.Collapsed));
        }

        public static string ShowDialog(string caption, string res = null, string fallback = null)
        {
            var ib = new InputBox(caption, res);
            ib.ShowDialog();
            return ib.exitState ? ib.Result : fallback ?? res;
        }

        public static object ShowDialog(string caption, IEnumerable source)
        {
            var ib = new InputBox(caption, source);
            ib.ShowDialog();
            return ib.exitState ? ib.Res : null;
        }

        private void clickCancel(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void clickOk(object sender, RoutedEventArgs e)
        {
            exitState = true;
            if (type) Res = cbAnswer.SelectedItem;
            Close();
        }
    }
}
