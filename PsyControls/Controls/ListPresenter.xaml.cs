﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace meet.wpf.controls.Controls
{
    /// <summary>
    /// Логика взаимодействия для ListPresenter.xaml
    /// </summary>
    public partial class ListPresenter
    {
        public enum EventType
        {
            Add, Remove, Clear
        }

        public struct EventArgs
        {
            public readonly EventType Type;
            public readonly int Index;

            public EventArgs(int index, EventType type)
            {
                Index = index;
                Type = type;
            }
        }

        public static string Add { get; set; }
        public static string Remove { get; set; }
        public static string Clear { get; set; }

        public event EventHandler<EventArgs> SourceChanged;

        private IList _itemsSource;
        public string Header { get; set; }
        public bool CanProvideMultipleItems { get; set; }
        public Func<object> CreateProvider { get; set; }
        public IList ItemsSource
        {
            get => _itemsSource;
            set {
                _itemsSource = value;
                lvInner.ItemsSource = _itemsSource;
                RefreshView();
            }
        }

        public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register(nameof(Header), typeof(string), typeof(ListPresenter));

        public event EventHandler<MouseButtonEventArgs> ItemDoubleClicked;
        public object SelectedItem => lvInner.SelectedItem;

        public void RefreshView()
        {
            lvInner.Items.Refresh();
        }

        public Dictionary<string, RoutedEventHandler> CustomButtons
        {
            set {
                value.Select(x => {
                    var (key, handler) = x;
                    var btn = new Button {
                        Margin = new Thickness(4),
                        Padding = new Thickness(8, 0, 8, 0),
                        Content = key
                    };
                    btn.Click += handler;
                    return btn;
                }).ToList().ForEach(x => spControls.Children.Add(x));
            }
        }

        public int SelectedIndex
        {
            get => lvInner.SelectedIndex;
            set => lvInner.SelectedIndex = value;
        }

        public ListPresenter()
        {
            InitializeComponent();
            lvInner.MouseDoubleClick += (s, e) => { ItemDoubleClicked?.Invoke(this, e); };
        }

        private void clickAdd(object sender, RoutedEventArgs e)
        {
            var t = CreateProvider?.Invoke();
            if (t is null) {
                Debug.WriteLine("[Error] create provider is null");
                return;
            }
            if (CanProvideMultipleItems && t is IEnumerable ie && !(t is string))
                foreach (var a in ie)
                    _itemsSource.Add(a);
            else
                _itemsSource.Add(t);
            SourceChanged?.Invoke(this, new EventArgs (_itemsSource.Count, EventType.Add));
            RefreshView();
        }

        private void clickRemove(object sender, RoutedEventArgs e)
        {
            if (lvInner.SelectedItem is null) return;
            var i = SelectedIndex;
            _itemsSource.Remove(lvInner.SelectedItem);
            SourceChanged?.Invoke(this, new EventArgs(i, EventType.Remove));
            RefreshView();
        }

        private void clickClear(object sender, RoutedEventArgs e)
        {
            _itemsSource.Clear();
            SourceChanged?.Invoke(this, new EventArgs(-1, EventType.Clear));
            RefreshView();
        }
    }
}
