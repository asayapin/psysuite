﻿using System;
using System.Text;
using System.Windows;
using meet.models;

namespace meet.wpf.controls.Controls.AnswerControls
{
    /// <summary>
    /// Логика взаимодействия для FreeAnswerInput.xaml
    /// </summary>
    public partial class FreeAnswerInput
    {
        public event EventHandler<ResultEventArgs> AnswerObtained;

        public static string Answer { get; set; }

        public FreeAnswerInput()
        {
            InitializeComponent();
        }

        private void clickAnswer(object sender, RoutedEventArgs e)
        {
            Tag = Encoding.UTF8.GetBytes(tbInput.Text);
            AnswerObtained?.Invoke(this,new ResultEventArgs(ResultType.Text));
        }
    }
}
