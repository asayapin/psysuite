﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using meet.models;

namespace meet.wpf.controls.Controls.AnswerControls
{
    /// <summary>
    /// Логика взаимодействия для DrawingInput.xaml
    /// </summary>
    public partial class DrawingInput
    {
        internal enum Mode
        {
            None, Draw, Erase
        }

        public static string Pencil { get; set; }
        public static string Erase { get; set; }
        public static string Clear { get; set; }
        public static string Answer { get; set; }

        public event EventHandler<ResultEventArgs> AnswerObtained;
        private Mode DrawMode;
        private bool isEnabled;
        private Point prevpos;
        private readonly Rectangle eraser;
        public DrawingInput()
        {
            InitializeComponent();
            eraser = new Rectangle {
                Stroke = Brushes.Black,
                Fill = Brushes.White,
                Width = 10,
                Height = 10,
                Margin = new Thickness {
                    Left = -20,
                    Top = -20
                },
                Visibility = Visibility.Collapsed
            };
            cvDrawing.Children.Add(eraser);
        }

        private void clickPencil(object sender, RoutedEventArgs e)
        {
            btPencil.BorderThickness = new Thickness(2);
            btEraser.BorderThickness = new Thickness(0);
            DrawMode = Mode.Draw;
            eraser.Visibility = Visibility.Collapsed;
        }

        private void clickEraser(object sender, RoutedEventArgs e)
        {
            btEraser.BorderThickness = new Thickness(2);
            btPencil.BorderThickness = new Thickness(0);
            DrawMode = Mode.Erase;
            eraser.Visibility = Visibility.Visible;
        }

        private void clickClear(object sender, RoutedEventArgs e)
        {
            cvDrawing.Children.Clear();
            btPencil.BorderThickness = new Thickness(0);
            btEraser.BorderThickness = new Thickness(0);
            DrawMode = Mode.None;
            eraser.Visibility = Visibility.Collapsed;
        }

        private void clickAnswer(object sender, RoutedEventArgs e)
        {
            var sz = new Size(cvDrawing.ActualWidth, cvDrawing.ActualHeight);
            cvDrawing.Measure(sz);
            cvDrawing.Arrange(new Rect(sz));

            int w = (int)sz.Width, h = (int)sz.Height;

            var rtb = new RenderTargetBitmap(w, h, 96d, 96d, PixelFormats.Pbgra32);
            rtb.Render(cvDrawing);

            var bt = new byte[w * h * 4 + 4];
            rtb.CopyPixels(bt, w*4, 4);

            BitConverter.GetBytes((short)w).CopyTo(bt, 0);
            BitConverter.GetBytes((short)h).CopyTo(bt, 2);

            Tag = bt;
            AnswerObtained?.Invoke(this, new ResultEventArgs(ResultType.Drawing));
        }

        private void CvDrawing_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!isEnabled && DrawMode != Mode.Erase) return;
            var p = e.GetPosition(cvDrawing);
            switch (DrawMode) {
                case Mode.Draw:
                    cvDrawing.Children.Add(new Line {
                        X1 = prevpos.X,
                        Y1 = prevpos.Y,
                        X2 = p.X,
                        Y2 = p.Y,
                        StrokeThickness = 1.2,
                        Stroke = Brushes.Black
                    });
                    break;
                case Mode.Erase:
                    eraser.Margin = new Thickness(p.X - eraser.Width / 2, p.Y - eraser.Height / 2, 0, 0);
                    if (isEnabled)
                        cvDrawing.Children.OfType<Line>().ToList().ForEach(x => {
                            if (Crosses(eraser, x))
                                cvDrawing.Children.Remove(x);
                        });
                    break;
            }

            prevpos = p;
        }
        public static bool Crosses(Rectangle rect, Line l)
        {
            var r = new Rect(rect.Margin.Left, rect.Margin.Top, rect.Width, rect.Height);
            return r.Contains(l.X1, l.Y1) || r.Contains(l.X2, l.Y2);
        }

        private void CvDrawing_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isEnabled = true;
            prevpos = e.GetPosition(cvDrawing);
        }

        private void CvDrawing_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isEnabled = false;
        }
    }
}
