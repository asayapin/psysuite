﻿using System;
using System.Windows.Input;
using System.Windows.Media;

namespace meet.wpf.controls.Controls
{
    /// <summary>
    /// Логика взаимодействия для QuestionPresenter.xaml
    /// </summary>
    public partial class QuestionPresenter
    {
        public static event EventHandler<string> ItemPressed;
        public QuestionPresenter(string content, bool state = false)
        {
            InitializeComponent();
            tbQuestion.Text = content;
            ellMark.Fill = state ? Brushes.LimeGreen : Brushes.Red;
        }

        public void SetState(bool state)
        {
            ellMark.Fill = state ? Brushes.LimeGreen : Brushes.Red;
        }

        private void QuestionPresenter_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                ItemPressed?.Invoke(sender, tbQuestion.Text);
        }
    }
}
