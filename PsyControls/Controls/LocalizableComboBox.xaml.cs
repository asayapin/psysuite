﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace meet.wpf.controls.Controls
{
    /// <summary>
    /// Логика взаимодействия для LocalizableComboBox.xaml
    /// </summary>
    public partial class LocalizableComboBox
    {
        private readonly Dictionary<string, object> items = new Dictionary<string, object>();
        private readonly List<string> innerSource = new List<string>();

        public LocalizableComboBox()
        {
            InitializeComponent();
            cbInner.ItemsSource = innerSource;
        }

        public string LocalizationPrefix { get; set; }
        public Type ResourcesType { get; set; }

        public void RefreshItems() => cbInner.Items.Refresh();

        public IEnumerable ItemsSource
        {
            get => items.Values;
            set
            {
                items.Clear();
                innerSource.Clear();
                foreach (var a in value)
                {
                    var s = GetLocalizedString(a);
                    innerSource.Add(s);
                    items.Add(s, a);
                }
                RefreshItems();
            }
        }

        public int SelectedIndex
        {
            get => cbInner.SelectedIndex;
            set => cbInner.SelectedIndex = value;
        }

        public object SelectedItem
        {
            get => items.Values.ElementAtOrDefault(cbInner.SelectedIndex);
            set => cbInner.SelectedIndex = value is null ? -1 : items.Values.ToList().FindIndex(value.Equals);
        }

        private string GetLocalizedString(object a)
        {
            var s = a.ToString();
            return ResourcesType.GetProperty($"{LocalizationPrefix}{s}")?.GetValue(null).ToString() ?? s;
        }

        public event EventHandler<SelectionChangedEventArgs> SelectionChanged;

        private void CbInner_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var sc = new SelectionChangedEventArgs(e.RoutedEvent, e.RemovedItems.OfType<string>().Select(x => items[x]).ToList(), e.AddedItems.OfType<string>().Select(x => items[x]).ToList());
            SelectionChanged?.Invoke(this, sc);
        }
    }
}
