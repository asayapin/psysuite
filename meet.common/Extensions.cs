﻿using System;

namespace meet.common
{
    public enum UserRole : uint
    {
        None, Admin = 1, Doctor, Patient
    }
    public static class Extensions
    {
        private static readonly byte[] PsyToken = { 207, 151, 103, 60, 136, 187, 4, 211, 177, 57, 181, 235, 158, 121, 252, 77, 90, 201, 168, 163, 128, 224, 169, 163, 124, 238, 223, 211, 175, 73, 28, 97, 229, 123, 81, 106, 163, 253, 144, 123, 196, 29, 186, 51, 48, 54, 91, 13, 62, 2, 57, 240, 53, 10, 26, 122, 47, 55, 226, 161, 114, 206, 8, 91 };
        public static ulong CreateKey(this UserRole role, uint id, uint duration = 2)
        {
            if (duration > 12) throw new NotSupportedException("duration is too big - max 12 hrs is supported");
            var now = (ulong)Math.Floor((DateTime.UtcNow.AddHours(duration) - DateTime.MinValue).TotalMinutes / 4);
            now = now * 10 + (uint)role;
            now = now * 100 + id;
            var key = BitConverter.GetBytes(now);
            var offset = 0;
            while (offset + key.Length < PsyToken.Length)
            {
                for (var i = 0; i < key.Length; i++)
                    key[i] += PsyToken[offset + i];
                offset += key.Length;
            }
            return BitConverter.ToUInt64(key, 0);
        }

        public static (UserRole, DateTime, uint) DecryptKey(this ulong key)
        {
            var bytes = BitConverter.GetBytes(key);
            var offset = 0;
            while (offset + bytes.Length < PsyToken.Length)
            {
                for (var i = 0; i < bytes.Length; i++)
                    bytes[i] -= PsyToken[offset + i];
                offset += bytes.Length;
            }

            var batch = BitConverter.ToUInt64(bytes, 0);
            var id = (uint) (batch % 100);
            batch /= 100U;
            var role = (UserRole)(batch % 10);
            // ReSharper disable once PossibleLossOfFraction
            var time = DateTime.MinValue + TimeSpan.FromMinutes((batch * 4) / 10);
            return (role, time, id);
        }
    }
}
