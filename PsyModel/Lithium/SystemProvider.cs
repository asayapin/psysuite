﻿using System;
using System.Reflection;
using lithium;

namespace meet.models.Lithium
{
    public class SystemProvider : ISystemProvider
    {
        public void Print(object o)
        {
            Console.WriteLine(o);
        }

        public void Print(string format, object[] os)
        {
            Console.WriteLine(format, os);
        }

        public string Input(string prompt)
        {
            Console.WriteLine(prompt);
            return Console.ReadLine();
        }

        public bool LoadAssembly(string path)
        {
            try
            {
                Assembly.LoadFrom(path);
                return true;
            }
            catch (Exception ex)
            {
                Print(ex);
                return false;
            }
        }
    }
}
