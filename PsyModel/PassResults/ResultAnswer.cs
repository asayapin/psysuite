﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using meet.models.Db;

namespace meet.models.PassResults
{
    [Serializable]
    public class ResultAnswer : Master
    {
        public int Question { get; set; }
        public ResultType ResultType { get; set; }
        public byte[] Value { get; set; } = new byte[0];
        public int GetValue()
        {
            if (ResultType != ResultType.Variant) return -1;
            switch (Value.Length) {
                case 1: return Value[0];
                case 2: return BitConverter.ToInt16(Value, 0);
                case 4: return BitConverter.ToInt32(Value, 0);
                default:
                    return -1;
            }
        }

        public override string ToString()
        {
            switch (ResultType) {
                case ResultType.Variant:
                    return GetValue().ToString();
                case ResultType.Text:
                    return string.Format(Resources.format_ResultAnswer_freeTS, Encoding.UTF8.GetString(Value));
                case ResultType.Drawing:
                    return Resources.itemConst_drawingAnswer;
            }
            return "";
        }

        public byte[] Serialize()
        {
            var xs = new XmlSerializer(typeof(ResultAnswer));
            using (var ms = new MemoryStream())
            {
                xs.Serialize(ms, this);
                return ms.ToArray();
            }
        }

        public static ResultAnswer Deserialize(byte[] src)
        {
            var xs = new XmlSerializer(typeof(ResultAnswer));
            using (var ms = new MemoryStream(src))
            {
                if (xs.Deserialize(ms) is ResultAnswer ra)
                    return ra;
            }
            return null;
        }
    }
}