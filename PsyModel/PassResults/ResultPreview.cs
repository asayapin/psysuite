﻿using System;
using meet.models.Db;

namespace meet.models.PassResults
{
    public class ResultPreview
    {
        internal long Id;
        public string Patient { get; set; }
        public string Methodic { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public static ResultPreview FromResult(MethodicResult mr) => new ResultPreview
        {
            Id = mr.Id,
            Patient = mr.Patient?.ToString(),
            Methodic = mr.MethodicName,
            Timestamp = new DateTimeOffset(mr.PassTimestamp, TimeSpan.FromMinutes(mr.UtcOffset))
        };

        public MethodicResult AsResult(MethodicResultService dp) => dp.Find(Id);
    }
}
