﻿using System;
using System.Collections.Generic;
using meet.models.Auth;
using meet.models.Db;
using Newtonsoft.Json;

namespace meet.models.PassResults
{
    public class MethodicResult : Master, IConvertible<MethodicResultDbProxy>
    {
        public string MethodicName { get; set; }
        public Patient Patient { get; set; }
        public User Doctor { get; set; }
        public List<ResultAnswer> Answers { get; set; } = new List<ResultAnswer>();
        public DateTime PassTimestamp { get; set; } = DateTime.UtcNow;
        public int UtcOffset { get; set; } = (int)DateTimeOffset.Now.Offset.TotalMinutes;
        public MethodicResultDbProxy Unwrap()
        {
            return new MethodicResultDbProxy
            {
                MethodicName = MethodicName,
                Answers = JsonConvert.SerializeObject(Answers),
                Id = Id,
                Doctor = Doctor,
                PassTimestamp = PassTimestamp,
                Patient = Patient,
                UtcOffset = UtcOffset
            };
        }

        public override string ToString()
        {
            return $"{Patient} - {MethodicName} - {new DateTimeOffset(PassTimestamp, TimeSpan.FromMinutes(UtcOffset))}";
        }

        public string GetPassTime() => new DateTimeOffset(PassTimestamp, TimeSpan.FromMinutes(UtcOffset)).ToString();
    }

    public class MethodicResultDbProxy : Master, IConvertible<MethodicResult>
    {
        public string MethodicName { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual User Doctor { get; set; }
        public string Answers { get; set; }
        public DateTime PassTimestamp { get; set; } = DateTime.UtcNow;
        public int UtcOffset { get; set; } = (int)DateTimeOffset.Now.Offset.TotalMinutes;

        public MethodicResult Unwrap()
        {
            return new MethodicResult
            {
                MethodicName = MethodicName,
                Answers = JsonConvert.DeserializeObject<List<ResultAnswer>>(Answers),
                Id = Id,
                Doctor = Doctor,
                PassTimestamp = PassTimestamp,
                Patient = Patient,
                UtcOffset = UtcOffset
            };
        }
    }
}