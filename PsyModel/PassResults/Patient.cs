﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using meet.models.Annotations;
using meet.models.Auth;
using meet.models.Db;

namespace meet.models.PassResults
{
    public class Patient : Master, ILoadable, INotifyPropertyChanged
    {
        private int _age;
        private bool _gender;
        public virtual User User { get; set; }

        public int Age
        {
            get => _age;
            set
            {
                if (value == _age) return;
                _age = value;
                OnPropertyChanged();
            }
        }

        public bool Gender
        {
            get => _gender;
            set
            {
                if (value == _gender) return;
                _gender = value;
                OnPropertyChanged();
            }
        }

        public virtual List<MethodicResultDbProxy> PassedMethodics { get; set; } = new List<MethodicResultDbProxy>();
        public void Load(ILoadable src)
        {
            if (src.GetType() == GetType()) {
                var s = (Patient)src;
                if (PassedMethodics is null)
                    PassedMethodics = new List<MethodicResultDbProxy>();
                PassedMethodics.Clear();
                if (!(s.PassedMethodics is null))
                    PassedMethodics.AddRange(s.PassedMethodics);
                Age = s.Age;
                Gender = s.Gender;
                if (User is null) User = s.User;
                else
                {
                    User.Identifier = s.User.Identifier;
                    User.Id = s.User.Id;
                    User.Name = s.User.Name;
                    User.Role = s.User.Role;
                    User.Results.Clear();
                    User.Results.AddRange(s.User.Results);
                }
            }
        }

        public ILoadable Copy()
        {
            var t = new Patient();
            t.Load(this);
            return t;
        }

        public override string ToString()
        {
            return string.Format(Resources.format_Patient_TS, User.Identifier, "");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
