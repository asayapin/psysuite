﻿using meet.common;
using meet.models.Auth;
using Microsoft.EntityFrameworkCore;

namespace meet.models.Db
{
    public class UsersProvider : AbstractProvider<User>
    {
        public UsersProvider(UserRole role) : base(role)
        {
        }

        protected override DbSet<User> GetSet(DbContext context)
        {
            return (context as DataContext)?.Users;
        }

        protected override DbContext GetContext()
        {
            return new DataContext();
        }
    }
}
