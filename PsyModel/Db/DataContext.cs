﻿using System.Configuration;
using meet.models.Auth;
using meet.models.Methodical.DbProxies;
using meet.models.PassResults;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace meet.models.Db
{
    internal class DataContext : DbContext
    {
        private static readonly ILoggerFactory LoggerFactory = Microsoft.Extensions.Logging.LoggerFactory.Create(builder => builder.AddDebug());

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlite(ConfigurationManager.ConnectionStrings["psyContext"].ConnectionString)
                .UseLoggerFactory(LoggerFactory);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<MethodicResultDbProxy> MethodicResults { get; set; }
        public DbSet<MethodicDbPx> Methodics { get; set; }
        public DbSet<Patient> Patients { get; set; }
    }
}