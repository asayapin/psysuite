﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using meet.common;
using meet.models.Auth;
using meet.models.Methodical.DbProxies;
using meet.models.Methodical.Representational;
using meet.models.PassResults;
using Microsoft.EntityFrameworkCore;

namespace meet.models.Db
{
    public abstract class AbstractProvider<T> where T : Master
    {
        protected AbstractProvider(UserRole role)
        {
            FillPermissions(role, _permissions);
        }

        #region CRUD templates

        protected abstract DbSet<T> GetSet(DbContext context);
        protected abstract DbContext GetContext();

        protected virtual IQueryable<T> PostGet(DbSet<T> set)
        {
            return set;
        }
        
        protected virtual TR RunInContext<TR>(Actions act, Func<DbSet<T>, TR> action, bool save = true)
        {
            CheckAccess(act);
            using var ctx = GetContext();
            var set = GetSet(ctx);
            var result = action(set);
            if (save) ctx.SaveChanges();
            return result;

        }

        protected void CheckAccess(Actions act)
        {
            if (_permissions.TryGetValue(typeof(T).AssemblyQualifiedName ?? throw new NullReferenceException("Type parameter cannot be null (but is)"), out var actions))
            {
                if (!actions.Contains(act))
                    throw new SecurityException($"Action {act} is not permitted");
            } else throw new SecurityException($"Type {typeof(T)} is not accessible");
        }

        #region create

        public virtual long Insert(T item) => RunInContext(Actions.Create, set => set.Add(item).Entity.Id);

        public void InsertOrUpdate(T item, Func<T, object> keySelector, Func<object, object, bool> predicate)
        {
            CheckAccess(Actions.Read);
            var itemKey = keySelector(item);
            var target = Find(x => predicate(itemKey, keySelector(x))).FirstOrDefault();
            if (target is null)
            {
                CheckAccess(Actions.Create);
                Insert(item);
                return;
            }
            CheckAccess(Actions.Update);
            item.Id = target.Id;
            Update(item);
        }

        #endregion

        #region read

        public virtual List<T> GetItems() => RunInContext(Actions.Read, set => PostGet(set).ToList(), false);

        public virtual T Find(long id) => RunInContext(Actions.Read, set => PostGet(set).ToList().FirstOrDefault(x => x.Id == id), false);

        public virtual IEnumerable<T> Find(Func<T, bool> predicate) => RunInContext(Actions.Read, set => PostGet(set).ToList().Where(predicate), false);

        #endregion

        #region update

        public bool Update(T item)
        {
            return RunInContext(Actions.Update, set =>
            {
                try
                {
                    set.Attach(item).State = EntityState.Modified;
                    return true;
                }
                catch { return false; }
            });
        }

        #endregion

        #region delete

        public bool Delete(T item)
        {
            return RunInContext(Actions.Delete, set =>
            {
                try
                {
                    set.Attach(item).State = EntityState.Deleted;
                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }

        #endregion

        #endregion

        public void WithAttached(T item, Action<T> action, bool save = false)
        {
            CheckAccess(Actions.Read);
            using var ctx = GetContext();
            var set = GetSet(ctx);
            var i = set.Attach(item).Entity;
            action(i);
            if (save) ctx.SaveChanges();
        }
        public void WithAttached(T item, Action<T, DbContext> action)
        {
            CheckAccess(Actions.Read);
            using var ctx = GetContext();
            var set = GetSet(ctx);
            var i = set.Attach(item).Entity;
            action(i, ctx);
        }

        #region permissions management
        private readonly Dictionary<string, Actions[]> _permissions = new Dictionary<string, Actions[]>();
        private static void FillPermissions(UserRole role, IDictionary<string, Actions[]> permissions)
        {
            switch (role)
            {
                case UserRole.Admin:
                    AddCross(permissions, AllTypes.Append(typeof(Patient)), Actions.Create, Actions.Read, Actions.Update);
                    AddCross(permissions, AllTypes.Where(FilterNonResOrAnswer).Append(typeof(Patient)), Actions.Delete);
                    break;
                case UserRole.Doctor:
                    AddCross(permissions, new[] { typeof(MethodicResultDbProxy), typeof(MethodicDbPx), typeof(ResultAnswer), typeof(Patient), typeof(PackDbPx) }, Actions.Read);
                    AddCross(permissions, new[] { typeof(PackDbPx), typeof(Patient) }, Actions.Create);
                    AddCross(permissions, new[] { typeof(Patient) }, Actions.Update);
                    break;
                default:
                    // NOTE : defaults to patient
                    AddCross(permissions, new[] { typeof(MethodicResultDbProxy), typeof(ResultAnswer) }, Actions.Create, Actions.Update);
                    AddCross(permissions, new[] { typeof(MethodicDbPx), typeof(PackDbPx) }, Actions.Read);
                    break;
            }
        }

        private static bool FilterNonResOrAnswer(Type t) => !new[] { typeof(MethodicResultDbProxy), typeof(ResultAnswer) }.Contains(t);
        private static readonly Type[] AllTypes = {
            typeof(User),
            typeof(MethodicResultDbProxy),
            typeof(ResultAnswer),
            typeof(AnswerDbPx),
            typeof(Representation),
            typeof(HeaderDbPx),
            typeof(QuestionDbPx),
            typeof(PackDbPx),
            typeof(MethodicDbPx)
        };
        private static void AddCross(IDictionary<string, Actions[]> permissions, IEnumerable<Type> types, params Actions[] acts)
        {
            foreach (var a in types.Select(x => x?.AssemblyQualifiedName ?? ""))
                if (permissions.TryGetValue(a, out var t))
                    permissions[a] = t.Concat(acts).ToArray();
                else
                    permissions[a] = acts.ToArray();
        } 
        #endregion
    }
}
