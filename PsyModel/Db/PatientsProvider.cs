﻿using System.Linq;
using meet.common;
using meet.models.PassResults;
using Microsoft.EntityFrameworkCore;

namespace meet.models.Db
{
    public class PatientsProvider : AbstractProvider<Patient>
    {
        public PatientsProvider(UserRole role) : base(role)
        {
        }

        protected override DbSet<Patient> GetSet(DbContext context)
        {
            return (context as DataContext)?.Patients;
        }

        protected override DbContext GetContext()
        {
            return new DataContext();
        }

        protected override IQueryable<Patient> PostGet(DbSet<Patient> set)
        {
            return set.Include(pt => pt.User).Include(x => x.PassedMethodics);
        }
    }
}
