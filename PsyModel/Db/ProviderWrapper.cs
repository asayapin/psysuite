﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;

namespace meet.models.Db
{
    public abstract class ProviderWrapper<TObject, TProxy> where TProxy : Master, IConvertible<TObject> where TObject : IConvertible<TProxy>
    {
        protected AbstractProvider<TProxy> Provider;

        public IEnumerable<TObject> GetItems() => Provider.GetItems().Select(x => x.Unwrap());

        public void InsertOrUpdate(TObject item, Func<TObject, object> keySelector,
            Func<object, object, bool> keyComparison)
        {
            var obj = item.Unwrap();
            Provider.InsertOrUpdate(obj, x => keySelector(x.Unwrap()), keyComparison);
        }

        public virtual long Insert(TObject item) => Provider.Insert(item.Unwrap());

        public void WithAttached(TObject item, Action<TObject> action) => Provider.WithAttached(item.Unwrap(), proxy => action(proxy.Unwrap()));

        public TObject Find(long id) => Provider.Find(id).Unwrap();

        public IEnumerable<TObject> Find(Func<TObject, bool> selector) =>
            Provider.Find(proxy => selector(proxy.Unwrap())).Select(x => x.Unwrap());
    }

    public interface IConvertible<out TTarget>
    {
        TTarget Unwrap();
    }
}
