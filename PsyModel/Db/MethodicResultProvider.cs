﻿using System;
using System.Diagnostics;
using System.Linq;
using meet.common;
using meet.models.PassResults;
using Microsoft.EntityFrameworkCore;

namespace meet.models.Db
{
    internal class MethodicResultProvider : AbstractProvider<MethodicResultDbProxy>
    {
        public MethodicResultProvider(UserRole role) : base(role)
        {
        }

        protected override DbSet<MethodicResultDbProxy> GetSet(DbContext context)
        {
            return (context as DataContext)?.MethodicResults;
        }

        protected override DbContext GetContext()
        {
            return new DataContext();
        }

        protected override IQueryable<MethodicResultDbProxy> PostGet(DbSet<MethodicResultDbProxy> set)
        {
            return set.Include(mr => mr.Patient).ThenInclude(p => p.User).Include(mr => mr.Doctor);
        }
    }

    public class MethodicResultService : ProviderWrapper<MethodicResult, MethodicResultDbProxy>
    {
        public MethodicResultService(UserRole role)
        {
            Provider = new MethodicResultProvider(role);
        }

        public override long Insert(MethodicResult item)
        {
            try
            {
                using (var ctx = new DataContext())
                {
                    var u = item.Unwrap();
                    ctx.Attach(u.Doctor);
                    ctx.Attach(u.Patient);
                    ctx.MethodicResults.Add(u);
                    ctx.SaveChanges();
                    return u.Id;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return 0;
            }
        }
    }
}
