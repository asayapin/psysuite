﻿using meet.common;
using meet.models.Methodical;
using meet.models.Methodical.DbProxies;
using Microsoft.EntityFrameworkCore;

namespace meet.models.Db
{
    internal class MethodicsProvdier : AbstractProvider<MethodicDbPx>
    {
        public MethodicsProvdier(UserRole role) : base(role)
        {
        }

        protected override DbSet<MethodicDbPx> GetSet(DbContext context)
        {
            return (context as DataContext)?.Methodics;
        }

        protected override DbContext GetContext()
        {
            return new DataContext();
        }
    }

    public class MethodicsService : ProviderWrapper<Methodic, MethodicDbPx>
    {
        public MethodicsService(UserRole role)
        {
            Provider = new MethodicsProvdier(role);
        }
    }
}
