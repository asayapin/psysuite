﻿using System;
using System.Collections.Generic;

namespace meet.models.Methodical.Representational
{
    [Serializable]
    public class Representation : ILoadable
    {
        public List<SubRepresentation> Representations { get; set; }= new List<SubRepresentation>();
        public void Load(ILoadable src)
        {
            if (!(src is Representation r)) return;
            if (Representations is null)
                Representations = new List<SubRepresentation>();
            if (r.Representations is null)
                return;
            Representations.AddRange(r.Representations);
        }

        public ILoadable Copy()
        {
            var t = new Representation();
            t.Load(this);
            return t;
        }
    }
}