﻿using System;
using System.Collections.Generic;

namespace meet.models.Methodical.Representational
{
    [Serializable]
    public class GraphicalRepresentation : SubRepresentation
    {
        public List<string> Values { get; set; } = new List<string>();
        public new bool IsPlain => true;
        public ScaleType ScaleType { get; set; }
        public int GraphType { get; set; }

        public override string ToString()
        {
            return string.Format(Resources.format_GraphicalRepresentation_TS, base.ToString());
        }

        public override void Load(ILoadable src)
        {
            if (!(src is GraphicalRepresentation gr)) return;
            Name = gr.Name;
            ScaleType = gr.ScaleType;
            GraphType = gr.GraphType;
            if (Values is null)
                Values = new List<string>();
            Values.Clear();
            if (gr.Values is null) return;
            Values.AddRange(gr.Values);
        }

        public override ILoadable Copy()
        {
            var t = new GraphicalRepresentation();
            t.Load(this);
            return t;
        }
    }
}