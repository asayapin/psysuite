﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using lithium;
using meet.models.Db;
using meet.models.Lithium;
using meet.models.PassResults;

namespace meet.models.Methodical.Representational
{
    public struct PatternItem
    {
        public readonly int[] Items;
        
        public PatternItem(string s)
        {
            Items = s.Trim('(', ')').Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.TryParse(x, out var t) ? t : -1).ToArray();
        }

        public bool Contains(int x) => Items.Contains(x);

        public static bool operator >(PatternItem pi, int x) => pi.Items.Length == 1 && pi.Items[0] > x;
        public static bool operator <(PatternItem pi, int x) => pi.Items.Length == 1 && pi.Items[0] < x;
        public static bool operator >(int x, PatternItem pi) => pi.Items.Length == 1 && pi.Items[0] < x;
        public static bool operator <(int x, PatternItem pi) => pi.Items.Length == 1 && pi.Items[0] > x;

        public override string ToString()
        {
            return $"({string.Join(" ", Items)})";
        }

        private static readonly Regex Matcher = new Regex(@"(\d+)|(\(.+?\))");

        public static List<PatternItem> SafeParse(string source) => Matcher.Matches(source).Where(x => !(x is null)).Select(x => new PatternItem(x.Value)).ToList();
    }

    [Serializable]
    public class PlainRepresentation : SubRepresentation, INotifyPropertyChanged
    {
        private static LiteLisp _engine = new LiteLisp(new SystemProvider());
        internal Host host = new Host();

        private static readonly ConcurrentDictionary<string, object> EvalCache = new ConcurrentDictionary<string, object>();

        public new bool IsPlain => true;
        public class Host
        {
            public double Value, Whole;
            public List<int> Answers;

            public object[] ToArgs() => new object[]{Value, Whole, Answers};

            public void InjectToEngine(LiteLisp eng)
            {
                eng.SetData(nameof(Value), DynamicAtom.Wrap(Value));
                eng.SetData(nameof(Whole), DynamicAtom.Wrap(Whole));
                eng.SetData(nameof(Answers), DynamicAtom.Wrap(Answers));
            }
        }

        private bool _isVisible;
        private string _postProcess;

        public bool IsVisible
        {
            get => _isVisible;
            set {
                if (value == _isVisible) return;
                _isVisible = value;
                OnPropertyChanged();
            }
        }

        public string PostProcess
        {
            get => _postProcess;
            set {
                if (value == _postProcess) return;
                _postProcess = value;
                OnPropertyChanged();
            }
        }

        public string Pattern { get; set; } = "";
        public List<string> Tags { get; set; } = new List<string>();
        public List<int> Questions { get; set; } = new List<int>();
        public MatchType MatchType { get; set; } = MatchType.Equal;
        public IEnumerable<int> RequiredQuestions(Methodic m)
        {
            var dict = m.Questions.Select((x, i) => new Tuple<int, Question>(i, x)).ToList();
            var byq = dict.Where(x => Questions.Contains(x.Item1)).Select(x => x.Item1);
            var byt = dict.Where(x => Tags.Intersect(x.Item2.Tags).Any()).Select(x => x.Item1);
            return byq.Union(byt).Distinct().ToList();
        }

        public double Value(Methodic m, MethodicResult mr, MethodicResultService dp)
        {
            var key = $"{mr}/{Name}";
            if (EvalCache.TryGetValue(key, out var res))
                return Convert(res);

            var Answers = new List<ResultAnswer>();
            try {
                Answers.AddRange(mr.Answers);
            } catch {
                try {
                    dp.WithAttached(mr, result => Answers.AddRange(result.Answers));
                } catch (Exception ex) {
                    Debug.WriteLine(ex.Message);
                }
            }
            var ss = RequiredQuestions(m);
            double retval;
            var ptr = PatternItem.SafeParse(Pattern);
            var answers = Answers.Where((x, i) => ss.Contains(i)).Where(x => x.ResultType == ResultType.Variant).ToList();
            switch (MatchType) {
                case MatchType.Equal:
                    retval = answers.Zip(ptr, (l, r) => r.Contains(l.GetValue()) ? 1 : 0).Sum();
                    break;
                case MatchType.Greater:
                    retval = answers.Zip(ptr, (l, r) => l.GetValue() > r ? 1 : 0).Sum();
                    break;
                case MatchType.Less:
                    retval = answers.Zip(ptr, (l, r) => l.GetValue() < r ? 1 : 0).Sum();
                    break;
                case MatchType.ByValue:
                    retval = answers.Sum(x => x.GetValue());
                    break;
                default:
                    EvalCache[key] = -1;
                    return -1;
            }

            if (string.IsNullOrWhiteSpace(PostProcess))
            {
                EvalCache[key] = retval;
                return retval;
            }
            host.Value = retval;
            host.Whole = Whole(m);
            host.Answers = Answers.Select(x => x.GetValue()).ToList();
            try
            {
                var pp = ProcessReferences(PostProcess, m, mr, dp);
                host.InjectToEngine(_engine);
                var eval = _engine.Eval(pp).Value(_engine);
                EvalCache[key] = eval;
                return Convert(eval);
            } catch (Exception e) {
                Debug.WriteLine(e.Message);
                EvalCache[key] = 0;
                return 0;
            }
        }

        private static double Convert(object o)
        {
            try
            {
                switch (o)
                {
                    case int i: return i;
                    case double d: return d;
                    default: return (double)o;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return 0;
            }
        }

        private string ProcessReferences(string pp, Methodic m, MethodicResult mr, MethodicResultService dp)
        {
            var dict = m.Representation.Representations.OfType<PlainRepresentation>().ToDictionary(x => $":{x.Name}:", x => x);
            foreach (var (key, value) in dict) {
                while (pp.Contains(key))
                    pp = pp.Replace(key, value.Value(m, mr, dp).ToString("0.##", CultureInfo.InvariantCulture));
            }
            return pp;
        }

        public double Whole(Methodic m)
        {
            double whole;
            switch (MatchType) {
                case MatchType.Equal:
                case MatchType.Greater:
                case MatchType.Less:
                case MatchType.ByValue when m.AnswersType == AnswersType.YesNo:
                    whole = string.IsNullOrWhiteSpace(Pattern) ? RequiredQuestions(m).Count() : PatternItem.SafeParse(Pattern).Count;
                    break;
                case MatchType.ByValue when m.AnswersType == AnswersType.Scale:
                    whole = RequiredQuestions(m).Count() * (m.ScaleWidth - 1 + m.ScaleBase);
                    break;
                default:
                    return -1;
            }
            return whole;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            return string.Format(Resources.format_PlainRepresentation_TS, base.ToString());
        }

        public string Content(Methodic m, MethodicResult mr, MethodicResultService dp) => string.Format(Resources.format_PlainRepresentation_build, Name, Value(m, mr, dp).ToString("#.00"));

        public override void Load(ILoadable src)
        {
            if (!(src is PlainRepresentation pr)) return;
            Name = pr.Name;
            IsVisible = pr.IsVisible;
            PostProcess = pr.PostProcess;
            MatchType = pr.MatchType;
            Pattern = pr.Pattern;
            if (Questions is null)
                Questions = new List<int>();
            Questions.Clear();
            if (!(pr.Questions is null))
                Questions.AddRange(pr.Questions);
            if (Tags is null)
                Tags = new List<string>();
            Tags.Clear();
            if (!(pr.Tags is null))
                Tags.AddRange(pr.Tags);
        }

        public override ILoadable Copy()
        {
            var t = new PlainRepresentation();
            t.Load(this);
            return t;
        }
    }
}