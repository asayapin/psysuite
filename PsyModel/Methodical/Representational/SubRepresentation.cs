﻿using System;
using meet.models.Db;
using meet.models.Methodical.DbProxies;

namespace meet.models.Methodical.Representational
{
    [Serializable]
    public class SubRepresentation : ILoadable, IConvertible<RepresentationDbPx>
    {
        public string Name { get; set; }
        public virtual bool IsPlain { get; }

        public RepresentationDbPx Unwrap()
        {
            switch (this)
            {
                case PlainRepresentation pr:
                    return new RepresentationDbPx
                    {
                        IsPlain = true,
                        EnumValue = (int)pr.MatchType,
                        IsVisible = pr.IsVisible,
                        Name = pr.Name,
                        PProc = pr.PostProcess,
                        QuestionsPx = string.Join(",", pr.Questions),
                        TagsPx = string.Join(",", pr.Tags),
                        ValuesPx = pr.Pattern
                    };
                case GraphicalRepresentation gr:
                    return new RepresentationDbPx
                    {
                        IsPlain = false,
                        Name = gr.Name,
                        EnumValue = gr.GraphType,
                        ValuesPx = string.Join(":", gr.Values)
                    };
                default: return null;
            }
        }

        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(Name) ? Resources.const_blankTS : Name;
        }

        public virtual void Load(ILoadable src)
        {
            if (src is SubRepresentation sr)
                Name = sr.Name;
        }

        public virtual ILoadable Copy()
        {
            var t = new SubRepresentation();
            t.Load(this);
            return t;
        }
    }
}