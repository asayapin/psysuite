﻿using System;
using System.Text;

namespace meet.models.Methodical
{
    [Serializable]
    public class Answer : ILoadable
    {
        public byte[] Data { get; set; } = new byte[0];
        public AnswerType AnswerType { get; set; }

        public override string ToString()
        {
            switch (AnswerType)
            {
                case AnswerType.Text:
                    return Encoding.UTF8.GetString(Data);
                case AnswerType.Picture:
                    return Resources.itemConst_picAnswer;
                case AnswerType.FreeText:
                    return Resources.itemConst_freeAnswer;
                case AnswerType.Drawing: return Resources.itemConst_drawingAnswer;
            }
            return "";
        }

        public void Load(ILoadable src)
        {
            if (!(src is Answer a)) return;
            Data = new byte[a.Data.Length];
            a.Data.CopyTo(Data, 0);
            AnswerType = a.AnswerType;
        }

        public ILoadable Copy()
        {
            var t = new Answer();
            t.Load(this);
            return t;
        }
    }

    public enum AnswerType
    {
        Text, Picture, FreeText, Drawing
    }
}