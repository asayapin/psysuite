﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;

namespace meet.models.Methodical
{
    [Serializable]
    public class MethodicPackProxy : ILoadable, INotifyPropertyChanged
    {
        private string _title;

        public string Title
        {
            get => _title;
            set
            {
                if (_title == value) return;
                _title = value;
                OnPropertyChanged();
            }
        }

        public List<string> Queue { get; set; } = new List<string>();
        public void Load(ILoadable src)
        {
            if (!(src is MethodicPackProxy mp)) return;
            if (Queue is null)
                Queue = new List<string>();
            Queue.Clear();
            if (mp.Queue is null) return;
            Queue.AddRange(mp.Queue);
        }

        public ILoadable Copy()
        {
            var t = new MethodicPackProxy();
            t.Load(this);
            return t;
        }

        public override string ToString()
        {
            return string.Format(Resources.format_MethodicPackProxy_TS, Title);
        }

        public static MethodicPackProxy Load(string path)
        {
            var xs = new XmlSerializer(typeof(MethodicPackProxy), Methodic.Knowns());
            using (var s = File.OpenRead(path))
                if ( xs.Deserialize(s) is MethodicPackProxy mpp)
                    return mpp;
            return null;
        }

        public void Write(string path)
        {
            var xs=  new XmlSerializer(typeof(MethodicPackProxy), Methodic.Knowns());
            using var s = File.Create(path);
            xs.Serialize(s, this);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}