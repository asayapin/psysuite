﻿using System.Collections.Generic;
using System.Linq;
using meet.models.Db;
using meet.models.Methodical.Representational;
using Newtonsoft.Json;

namespace meet.models.Methodical.DbProxies
{
    public class MethodicDbPx : Master, IConvertible<Methodic>
    {

        public Methodic Unwrap()
        {
            var meth = new Methodic
            {
                Id = (uint) Id,
                AnswersType = AnswersType,
                Title = Title,
                Code = Code,
                Description = Description,
                Instructions = Instructions,
                ScaleBase = ScaleBase,
                ScaleWidth = (uint) ScaleWidth,
                Locale = Locale,
                Representation = new Representation{Representations = JsonConvert.DeserializeObject<List<RepresentationDbPx>>(Representations).Select(x => x.Unwrap()).ToList()},
                Questions = JsonConvert.DeserializeObject<List<Question>>(QuestionsPx),
                Headers = JsonConvert.DeserializeObject<List<string>>(HeadersPx)
            };
            return meth;
        }

        public string HeadersPx { get; set; }

        public string QuestionsPx { get; set; }

        public string Representations { get; set; }

        public int ScaleWidth { get; set; }

        public int ScaleBase { get; set; }

        public string Instructions { get; set; }

        public string Description { get; set; }

        public string Code { get; set; }

        public string Title { get; set; }
        public string Locale { get; set; }

        public AnswersType AnswersType { get; set; }
        public override string ToString()
        {
            return Title;
        }
    }
}
