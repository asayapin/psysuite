﻿using meet.models.Db;

namespace meet.models.Methodical.DbProxies
{
    public class HeaderDbPx : Master
    {
        public string Value { get; set; }
        public override string ToString()
        {
            return Value;
        }
    }
}
