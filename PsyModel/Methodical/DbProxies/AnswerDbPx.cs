﻿using System.Linq;
using meet.models.Db;

namespace meet.models.Methodical.DbProxies
{
    public class AnswerDbPx : Master
    {
        public static AnswerDbPx FromAnswer(Answer a)
        {
            return new AnswerDbPx
            {
                AnswerType = a.AnswerType,
                Data = a.Data.ToArray()
            };
        }

        public Answer AsAnswer()
        {
            return new Answer
            {
                AnswerType = AnswerType,
                Data = Data
            };
        }

        public byte[] Data { get; set; }

        public AnswerType AnswerType { get; set; }
        public override string ToString()
        {
            return AsAnswer().ToString();
        }
    }
}
