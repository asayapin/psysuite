﻿using System;
using System.Linq;
using meet.models.Db;

namespace meet.models.Methodical.DbProxies
{
    public class PackDbPx : Master
    {
        public const string Delimiter = ":";

        public MethodicPackProxy AsPackProxy()
        {
            return new MethodicPackProxy
            {
                Title = Title, 
                Queue = Methodics.Split(Delimiter.ToCharArray(),StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList()
            };
        }

        public string Methodics { get; set; }

        public string Title { get; set; }

        public override string ToString()
        {
            return string.Format(Resources.format_MethodicPackProxy_TS, Title);
        }
    }
}
