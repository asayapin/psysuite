﻿using System;
using System.Linq;
using meet.models.Db;
using meet.models.Methodical.Representational;

namespace meet.models.Methodical.DbProxies
{
    public class RepresentationDbPx : Master, IConvertible<SubRepresentation>
    {

        private SubRepresentation FastRepr()
        {
            return IsPlain ? new PlainRepresentation{Name = Name} : (SubRepresentation)new GraphicalRepresentation{Name = Name};
        }

        public SubRepresentation Unwrap()
        {
            if (IsPlain)
                return new PlainRepresentation
                {
                    Name = Name, IsVisible = IsVisible,
                    MatchType = (MatchType)EnumValue,
                    Pattern = ValuesPx,
                    PostProcess = PProc,
                    Questions = QuestionsPx?.Split(new[]{',',' '}, StringSplitOptions.RemoveEmptyEntries).Select(x => int.TryParse(x.Trim(), out var a) ? a : -1).ToList(),
                    Tags = TagsPx?.Split(',').Select(x => x.Trim()).ToList()
                };
            return new GraphicalRepresentation
            {
                Values = ValuesPx?.Split(new[]{':'}, StringSplitOptions.RemoveEmptyEntries).ToList(),
                GraphType = EnumValue,
                Name = Name
            };
        }

        public string TagsPx { get; set; }

        public string QuestionsPx { get; set; }

        public string PProc { get; set; }

        public bool IsVisible { get; set; }

        public string Name { get; set; }

        public int EnumValue { get; set; }

        public string ValuesPx { get; set; }

        public bool IsPlain { get; set; }
        public override string ToString()
        {
            return FastRepr().ToString();
        }
    }
}
