﻿using System;
using System.Collections.Generic;
using System.Linq;
using meet.models.Db;
using Microsoft.EntityFrameworkCore;

namespace meet.models.Methodical.DbProxies
{
    public class QuestionDbPx : Master
    {
        public string TagsPx { get; set; }

        public string Text { get; set; }
        public int Order { get; set; }
        public byte[] Picture { get; set; }
        public PreferredLayout PreferredLayout { get; set; }

        public virtual List<AnswerDbPx> Answers { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
