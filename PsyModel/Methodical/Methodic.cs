﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using meet.models.Db;
using meet.models.Methodical.DbProxies;
using meet.models.Methodical.Representational;
using Newtonsoft.Json;

namespace meet.models.Methodical
{
    [Serializable]
    [KnownType(nameof(Knowns))]
    [XmlInclude(typeof(PlainRepresentation))]
    [XmlInclude(typeof(GraphicalRepresentation))]
    public class Methodic : ILoadable, INotifyPropertyChanged, IConvertible<MethodicDbPx>
    {
        private uint _id;
        private AnswersType _answersType;
        private uint _scaleWidth = 5;
        private string _code;
        private List<Question> _questions = new List<Question>();
        private string _title;
        private string _description;
        private string _instructions;
        private Representation _representation;
        private int _scaleBase;
        private string _locale = "ru";

        public uint Id
        {
            get => _id;
            set
            {
                if (value == _id) return;
                _id = value;
                OnPropertyChanged();
            }
        }

        public AnswersType AnswersType
        {
            get => _answersType;
            set
            {
                if (value == _answersType) return;
                _answersType = value;
                OnPropertyChanged();
            }
        }

        public uint ScaleWidth
        {
            get => _scaleWidth;
            set {
                if (value == _scaleWidth) return;
                _scaleWidth = value;
                OnPropertyChanged();
            }
        }
        public int ScaleBase
        {
            get => _scaleBase;
            set {
                if (value == _scaleBase) return;
                _scaleBase = value;
                OnPropertyChanged();
            }
        }

        public List<Question> Questions
        {
            get => _questions;
            set
            {
                if ( value.SequenceEqual(_questions)) return;
                _questions = value;
                OnPropertyChanged();
            }
        }

        public List<string> Headers { get; set; } = new List<string>();

        public string Code {
            get => _code;
            set {
                if (_code == value)
                    return;
                _code = value;
                OnPropertyChanged();
            }
        }

        public string Title
        {
            get => _title;
            set
            {
                if (value == _title) return;
                _title = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                if (value == _description) return;
                _description = value;
                OnPropertyChanged();
            }
        }

        public string Instructions
        {
            get => _instructions;
            set
            {
                if (value == _instructions) return;
                _instructions = value;
                OnPropertyChanged();
            }
        }

        public string Locale
        {
            get => _locale;
            set
            {
                if (_locale == value) return;
                _locale = value; 
                OnPropertyChanged();
            }
        }

        public Representation Representation
        {
            get => _representation;
            set
            {
                if (Equals(value, _representation)) return;
                _representation = value;
                OnPropertyChanged();
            }
        }

        public void Load(ILoadable src)
        {
            if (src.GetType() != GetType()) return;
            var s = (Methodic)src;
            Id = s.Id;
            AnswersType = s.AnswersType;
            ScaleWidth = s.ScaleWidth;
            ScaleBase = s.ScaleBase;
            Code = s.Code;
            Title = s.Title;
            Description = s.Description;
            Instructions = s.Instructions;
            Locale = s.Locale;
            if (Questions is null)
                Questions = new List<Question>();
            Questions.Clear();
            if (!(s.Questions is null))
                Questions.AddRange(s.Questions);
            if (Representation is null)
                Representation = new Representation();
            Representation.Representations.Clear();
            if (!(s.Representation?.Representations is null))
                Representation.Representations.AddRange(s.Representation.Representations);
            Headers.Clear();
            if (!(s.Headers is null))
                Headers.AddRange(s.Headers);
        }

        public ILoadable Copy()
        {
            var t = new Methodic();
            t.Load(this);
            return t;
        }
        
        public static Type[] Knowns() => KnownTypes().ToArray();
        internal static IEnumerable<Type> KnownTypes() => typeof(Methodic).Assembly.GetTypes().Where(OfSameNamespace).Where(IsSerializable).Where(IsAccessible);

        private static bool IsSerializable(Type x)
        {
            return x.IsSerializable;
        }

        private static bool IsAccessible(Type x)
        {
            return x.IsPublic;
        }

        private static bool OfSameNamespace(Type x)
        {
            return x.Namespace == typeof(Methodic).Namespace;
        }

        public MethodicDbPx Unwrap()
        {
            return new MethodicDbPx
            {
                Id = Id,
                AnswersType = AnswersType,
                Title = Title,
                Code = Code,
                Description = Description,
                Instructions = Instructions,
                ScaleWidth = (int)ScaleWidth,
                ScaleBase = ScaleBase,
                Locale = Locale,
                Representations = JsonConvert.SerializeObject(Representation.Representations.Select(x => x.Unwrap())),
                QuestionsPx = JsonConvert.SerializeObject(Questions),
                HeadersPx = JsonConvert.SerializeObject(Headers)
            };
        }

        public override string ToString()
        {
            return Title;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static Methodic Load(string path)
        {
            if (!File.Exists(path)) return null;
            var xs = new XmlSerializer(typeof(Methodic), Knowns());
            using (var s = File.OpenRead(path))
                if (xs.Deserialize(s) is Methodic mpp)
                {
                    if (mpp.Questions.Any(x => x.Order != 0)) return mpp;
                    for (var i = 0; i < mpp.Questions.Count; i++)
                        mpp.Questions[i].Order = i + 1;
                    return mpp.Representation.Representations.Select(x => x.Name).GroupBy(x => x).Any(x => x.Count() > 1) ? null : mpp;
                }
            return null;
        }

        public void Write(string path)
        {
            var xs = new XmlSerializer(typeof(Methodic), Knowns());
            using Stream s = File.Create(path);
            xs.Serialize(s, this);
        }
    }
}