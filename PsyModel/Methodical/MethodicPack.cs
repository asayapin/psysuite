﻿using System.Collections.Generic;
using System.IO;

namespace meet.models.Methodical
{
    public class MethodicPack : ILoadable
    {
        public Queue<Methodic> Queue { get; set; } = new Queue<Methodic>();
        internal MethodicPack()
        {
            
        }
        public static MethodicPack FromEnumerable(IEnumerable<Methodic> m) => new MethodicPack{Queue = new Queue<Methodic>(m)};
        public void Load(ILoadable src)
        {
            if (!(src is MethodicPack mp)) return;
            if (Queue is null)
                Queue = new Queue<Methodic>();
            Queue.Clear();
            if (mp.Queue is null) return;
            foreach (var a in mp.Queue)
                Queue.Enqueue(a);
        }
    }
}