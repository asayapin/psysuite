﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace meet.models.Methodical
{
[Serializable]
    public class Question : ILoadable, INotifyPropertyChanged
    {
        private string _text;

        public string Text
        {
            get => _text;
            set
            {
                if (value == _text) return;
                _text = value;
                OnPropertyChanged();
            }
        }
        public byte[] Picture { get; set; }
        public PreferredLayout PreferredLayout { get; set; }
        public List<Answer> Answers { get; set; } = new List<Answer>();
        public List<string> Tags { get; set; } = new List<string>();
        public int Order { get; set; }
        public void Load(ILoadable src)
        {
            if (src.GetType() != GetType()) return;
            var t = (Question)src;
            Text = t.Text;
            PreferredLayout = t.PreferredLayout;
            if (Answers is null)
                Answers = new List<Answer>();
            Answers.Clear();
            if (!(t.Answers is null))
                Answers.AddRange(t.Answers);
            if (Tags is null)
                Tags = new List<string>();
            Tags.Clear();
            if (!(t.Tags is null))
                Tags.AddRange(t.Tags);
            Picture = t.Picture?.ToArray() ?? new byte[0];
        }

        public ILoadable Copy()
        {
            var t = new Question();
            t.Load(this);
            return t;
        }

        public override string ToString()
        {
            return Text;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}