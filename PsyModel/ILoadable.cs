﻿namespace meet.models
{
    public interface ILoadable
    {
        void Load(ILoadable src);
    }
}