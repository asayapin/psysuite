﻿namespace meet.models
{
    public enum Actions
    {
        Read, Create, Delete, Update
    }
    public enum AnswersType
    {
        None,
        YesNo,
        Scale,
        Variants,
    }

    public enum ScaleType
    {
        // ReSharper disable UnusedMember.Global
        Linear, Logarithm
    }

    public enum MatchType
    {
        Equal, Greater, Less, ByValue
    }
    public enum PreferredLayout
    {
        Stack, Grid
    }

    public enum ResultType
    {
        Variant,
        Drawing,
        Text
    }
}
