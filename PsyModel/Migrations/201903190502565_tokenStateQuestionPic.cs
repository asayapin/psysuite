namespace PsyModel.Migrations
{
    public partial class tokenStateQuestionPic : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MethodicResults", "ExportState", c => c.Int(nullable: false));
            AddColumn("dbo.QuestionDbPxes", "Picture", c => c.Binary());
            AddColumn("dbo.Tokens", "State", c => c.Int(nullable: false));
            DropColumn("dbo.Tokens", "IsActive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tokens", "IsActive", c => c.Boolean(nullable: false));
            DropColumn("dbo.Tokens", "State");
            DropColumn("dbo.QuestionDbPxes", "Picture");
            DropColumn("dbo.MethodicResults", "ExportState");
        }
    }
}
