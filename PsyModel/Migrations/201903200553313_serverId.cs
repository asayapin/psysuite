namespace PsyModel.Migrations
{
    public partial class serverId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnswerDbPxes", "SrvId", c => c.Long(nullable: false));
            AddColumn("dbo.HeaderDbPxes", "SrvId", c => c.Long(nullable: false));
            AddColumn("dbo.MethodicResults", "SrvId", c => c.Long(nullable: false));
            AddColumn("dbo.ResultAnswers", "SrvId", c => c.Long(nullable: false));
            AddColumn("dbo.Users", "SrvId", c => c.Long(nullable: false));
            AddColumn("dbo.MethodicDbPxes", "SrvId", c => c.Long(nullable: false));
            AddColumn("dbo.QuestionDbPxes", "SrvId", c => c.Long(nullable: false));
            AddColumn("dbo.RepresentationDbPxes", "SrvId", c => c.Long(nullable: false));
            AddColumn("dbo.PackDbPxes", "SrvId", c => c.Long(nullable: false));
            AddColumn("dbo.Tokens", "SrvId", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tokens", "SrvId");
            DropColumn("dbo.PackDbPxes", "SrvId");
            DropColumn("dbo.RepresentationDbPxes", "SrvId");
            DropColumn("dbo.QuestionDbPxes", "SrvId");
            DropColumn("dbo.MethodicDbPxes", "SrvId");
            DropColumn("dbo.Users", "SrvId");
            DropColumn("dbo.ResultAnswers", "SrvId");
            DropColumn("dbo.MethodicResults", "SrvId");
            DropColumn("dbo.HeaderDbPxes", "SrvId");
            DropColumn("dbo.AnswerDbPxes", "SrvId");
        }
    }
}
