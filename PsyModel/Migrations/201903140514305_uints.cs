namespace PsyModel.Migrations
{
    public partial class uints : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MethodicDbPxes", "ScaleWidth", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MethodicDbPxes", "ScaleWidth");
        }
    }
}
