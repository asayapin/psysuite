// <auto-generated />
namespace PsyModel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class locale : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(locale));
        
        string IMigrationMetadata.Id
        {
            get { return "201903180500062_locale"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
