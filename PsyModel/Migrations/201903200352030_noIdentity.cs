namespace PsyModel.Migrations
{
    public partial class noIdentity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ResultAnswers", "MethodicResult_Id", "dbo.MethodicResults");
            DropForeignKey("dbo.MethodicResults", "Doctor_Id", "dbo.Users");
            DropForeignKey("dbo.MethodicResults", "Patient_Id", "dbo.Users");
            DropForeignKey("dbo.HeaderDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.QuestionDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.RepresentationDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.AnswerDbPxes", "QuestionDbPx_Id", "dbo.QuestionDbPxes");
            DropPrimaryKey("dbo.AnswerDbPxes");
            DropPrimaryKey("dbo.HeaderDbPxes");
            DropPrimaryKey("dbo.MethodicResults");
            DropPrimaryKey("dbo.ResultAnswers");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.MethodicDbPxes");
            DropPrimaryKey("dbo.QuestionDbPxes");
            DropPrimaryKey("dbo.RepresentationDbPxes");
            DropPrimaryKey("dbo.PackDbPxes");
            DropPrimaryKey("dbo.Tokens");
            AlterColumn("dbo.AnswerDbPxes", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.HeaderDbPxes", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.MethodicResults", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.ResultAnswers", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.Users", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.MethodicDbPxes", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.QuestionDbPxes", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.RepresentationDbPxes", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.PackDbPxes", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.Tokens", "Id", c => c.Long(nullable: false));
            AddPrimaryKey("dbo.AnswerDbPxes", "Id");
            AddPrimaryKey("dbo.HeaderDbPxes", "Id");
            AddPrimaryKey("dbo.MethodicResults", "Id");
            AddPrimaryKey("dbo.ResultAnswers", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.MethodicDbPxes", "Id");
            AddPrimaryKey("dbo.QuestionDbPxes", "Id");
            AddPrimaryKey("dbo.RepresentationDbPxes", "Id");
            AddPrimaryKey("dbo.PackDbPxes", "Id");
            AddPrimaryKey("dbo.Tokens", "Id");
            AddForeignKey("dbo.ResultAnswers", "MethodicResult_Id", "dbo.MethodicResults", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MethodicResults", "Doctor_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.MethodicResults", "Patient_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.HeaderDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.QuestionDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RepresentationDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AnswerDbPxes", "QuestionDbPx_Id", "dbo.QuestionDbPxes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswerDbPxes", "QuestionDbPx_Id", "dbo.QuestionDbPxes");
            DropForeignKey("dbo.RepresentationDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.QuestionDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.HeaderDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.MethodicResults", "Patient_Id", "dbo.Users");
            DropForeignKey("dbo.MethodicResults", "Doctor_Id", "dbo.Users");
            DropForeignKey("dbo.ResultAnswers", "MethodicResult_Id", "dbo.MethodicResults");
            DropPrimaryKey("dbo.Tokens");
            DropPrimaryKey("dbo.PackDbPxes");
            DropPrimaryKey("dbo.RepresentationDbPxes");
            DropPrimaryKey("dbo.QuestionDbPxes");
            DropPrimaryKey("dbo.MethodicDbPxes");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.ResultAnswers");
            DropPrimaryKey("dbo.MethodicResults");
            DropPrimaryKey("dbo.HeaderDbPxes");
            DropPrimaryKey("dbo.AnswerDbPxes");
            AlterColumn("dbo.Tokens", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.PackDbPxes", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.RepresentationDbPxes", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.QuestionDbPxes", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.MethodicDbPxes", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Users", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.ResultAnswers", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.MethodicResults", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.HeaderDbPxes", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.AnswerDbPxes", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.Tokens", "Id");
            AddPrimaryKey("dbo.PackDbPxes", "Id");
            AddPrimaryKey("dbo.RepresentationDbPxes", "Id");
            AddPrimaryKey("dbo.QuestionDbPxes", "Id");
            AddPrimaryKey("dbo.MethodicDbPxes", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.ResultAnswers", "Id");
            AddPrimaryKey("dbo.MethodicResults", "Id");
            AddPrimaryKey("dbo.HeaderDbPxes", "Id");
            AddPrimaryKey("dbo.AnswerDbPxes", "Id");
            AddForeignKey("dbo.AnswerDbPxes", "QuestionDbPx_Id", "dbo.QuestionDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RepresentationDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.QuestionDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.HeaderDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MethodicResults", "Patient_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.MethodicResults", "Doctor_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.ResultAnswers", "MethodicResult_Id", "dbo.MethodicResults", "Id", cascadeDelete: true);
        }
    }
}
