namespace PsyModel.Migrations
{
    public partial class qOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionDbPxes", "Order", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuestionDbPxes", "Order");
        }
    }
}
