namespace PsyModel.Migrations
{
    public partial class viewState : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnswerDbPxes", "ViewState", c => c.Int(nullable: false));
            AddColumn("dbo.HeaderDbPxes", "ViewState", c => c.Int(nullable: false));
            AddColumn("dbo.MethodicResults", "ViewState", c => c.Int(nullable: false));
            AddColumn("dbo.ResultAnswers", "ViewState", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "ViewState", c => c.Int(nullable: false));
            AddColumn("dbo.MethodicDbPxes", "ViewState", c => c.Int(nullable: false));
            AddColumn("dbo.QuestionDbPxes", "ViewState", c => c.Int(nullable: false));
            AddColumn("dbo.RepresentationDbPxes", "ViewState", c => c.Int(nullable: false));
            AddColumn("dbo.PackDbPxes", "ViewState", c => c.Int(nullable: false));
            AddColumn("dbo.Tokens", "ViewState", c => c.Int(nullable: false));
            DropColumn("dbo.MethodicResults", "ExportState");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MethodicResults", "ExportState", c => c.Int(nullable: false));
            DropColumn("dbo.Tokens", "ViewState");
            DropColumn("dbo.PackDbPxes", "ViewState");
            DropColumn("dbo.RepresentationDbPxes", "ViewState");
            DropColumn("dbo.QuestionDbPxes", "ViewState");
            DropColumn("dbo.MethodicDbPxes", "ViewState");
            DropColumn("dbo.Users", "ViewState");
            DropColumn("dbo.ResultAnswers", "ViewState");
            DropColumn("dbo.MethodicResults", "ViewState");
            DropColumn("dbo.HeaderDbPxes", "ViewState");
            DropColumn("dbo.AnswerDbPxes", "ViewState");
        }
    }
}
