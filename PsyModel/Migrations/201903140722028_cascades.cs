namespace PsyModel.Migrations
{
    public partial class cascades : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HeaderDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.QuestionDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.RepresentationDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.AnswerDbPxes", "QuestionDbPx_Id", "dbo.QuestionDbPxes");
            AddForeignKey("dbo.HeaderDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.QuestionDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RepresentationDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AnswerDbPxes", "QuestionDbPx_Id", "dbo.QuestionDbPxes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswerDbPxes", "QuestionDbPx_Id", "dbo.QuestionDbPxes");
            DropForeignKey("dbo.RepresentationDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.QuestionDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.HeaderDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            AddForeignKey("dbo.AnswerDbPxes", "QuestionDbPx_Id", "dbo.QuestionDbPxes", "Id");
            AddForeignKey("dbo.RepresentationDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id");
            AddForeignKey("dbo.QuestionDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id");
            AddForeignKey("dbo.HeaderDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes", "Id");
        }
    }
}
