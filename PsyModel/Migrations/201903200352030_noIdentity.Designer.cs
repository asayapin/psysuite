// <auto-generated />
namespace PsyModel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class noIdentity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(noIdentity));
        
        string IMigrationMetadata.Id
        {
            get { return "201903200352030_noIdentity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
