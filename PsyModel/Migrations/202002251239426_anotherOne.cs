namespace PsyModel.Migrations
{
    public partial class anotherOne : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "Role");
            DropColumn("dbo.Tokens", "Role");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tokens", "Role", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "Role", c => c.Int(nullable: false));
        }
    }
}
