namespace PsyModel.Migrations
{
    public partial class cascade2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ResultAnswers", "MethodicResult_Id", "dbo.MethodicResults");
            AddForeignKey("dbo.ResultAnswers", "MethodicResult_Id", "dbo.MethodicResults", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ResultAnswers", "MethodicResult_Id", "dbo.MethodicResults");
            AddForeignKey("dbo.ResultAnswers", "MethodicResult_Id", "dbo.MethodicResults", "Id");
        }
    }
}
