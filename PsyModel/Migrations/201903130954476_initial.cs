namespace PsyModel.Migrations
{
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerDbPxes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Data = c.Binary(),
                        AnswerType = c.Int(nullable: false),
                        QuestionDbPx_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionDbPxes", t => t.QuestionDbPx_Id)
                .Index(t => t.QuestionDbPx_Id);
            
            CreateTable(
                "dbo.HeaderDbPxes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Value = c.String(),
                        MethodicDbPx_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MethodicDbPxes", t => t.MethodicDbPx_Id)
                .Index(t => t.MethodicDbPx_Id);
            
            CreateTable(
                "dbo.MethodicResults",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MethodicName = c.String(),
                        PassTimestamp = c.DateTime(nullable: false),
                        UtcOffset = c.Int(nullable: false),
                        Patient_Id = c.Long(),
                        Doctor_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Patient_Id)
                .ForeignKey("dbo.Users", t => t.Doctor_Id)
                .Index(t => t.Patient_Id)
                .Index(t => t.Doctor_Id);
            
            CreateTable(
                "dbo.ResultAnswers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Question = c.Int(nullable: false),
                        ResultType = c.Int(nullable: false),
                        Value = c.Binary(),
                        MethodicResult_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MethodicResults", t => t.MethodicResult_Id)
                .Index(t => t.MethodicResult_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Identifier = c.String(),
                        Name = c.String(),
                        Role = c.Int(nullable: false),
                        Modulus = c.Binary(),
                        Exponent = c.Binary(),
                        DQ = c.Binary(),
                        DP = c.Binary(),
                        InverseQ = c.Binary(),
                        Q = c.Binary(),
                        P = c.Binary(),
                        D = c.Binary(),
                        Token = c.Binary(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MethodicDbPxes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ScaleBase = c.Int(nullable: false),
                        Instructions = c.String(),
                        Description = c.String(),
                        Code = c.String(),
                        Title = c.String(),
                        AnswersType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuestionDbPxes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TagsPx = c.String(),
                        Text = c.String(),
                        PreferredLayout = c.Int(nullable: false),
                        MethodicDbPx_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MethodicDbPxes", t => t.MethodicDbPx_Id)
                .Index(t => t.MethodicDbPx_Id);
            
            CreateTable(
                "dbo.RepresentationDbPxes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TagsPx = c.String(),
                        QuestionsPx = c.String(),
                        PProc = c.String(),
                        IsVisible = c.Boolean(nullable: false),
                        Name = c.String(),
                        EnumValue = c.Int(nullable: false),
                        ValuesPx = c.String(),
                        IsPlain = c.Boolean(nullable: false),
                        MethodicDbPx_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MethodicDbPxes", t => t.MethodicDbPx_Id)
                .Index(t => t.MethodicDbPx_Id);
            
            CreateTable(
                "dbo.PackDbPxes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Methodics = c.String(),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tokens",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Value = c.Binary(),
                        Role = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RepresentationDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.QuestionDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.AnswerDbPxes", "QuestionDbPx_Id", "dbo.QuestionDbPxes");
            DropForeignKey("dbo.HeaderDbPxes", "MethodicDbPx_Id", "dbo.MethodicDbPxes");
            DropForeignKey("dbo.MethodicResults", "Doctor_Id", "dbo.Users");
            DropForeignKey("dbo.MethodicResults", "Patient_Id", "dbo.Users");
            DropForeignKey("dbo.ResultAnswers", "MethodicResult_Id", "dbo.MethodicResults");
            DropIndex("dbo.RepresentationDbPxes", new[] { "MethodicDbPx_Id" });
            DropIndex("dbo.QuestionDbPxes", new[] { "MethodicDbPx_Id" });
            DropIndex("dbo.ResultAnswers", new[] { "MethodicResult_Id" });
            DropIndex("dbo.MethodicResults", new[] { "Doctor_Id" });
            DropIndex("dbo.MethodicResults", new[] { "Patient_Id" });
            DropIndex("dbo.HeaderDbPxes", new[] { "MethodicDbPx_Id" });
            DropIndex("dbo.AnswerDbPxes", new[] { "QuestionDbPx_Id" });
            DropTable("dbo.Tokens");
            DropTable("dbo.PackDbPxes");
            DropTable("dbo.RepresentationDbPxes");
            DropTable("dbo.QuestionDbPxes");
            DropTable("dbo.MethodicDbPxes");
            DropTable("dbo.Users");
            DropTable("dbo.ResultAnswers");
            DropTable("dbo.MethodicResults");
            DropTable("dbo.HeaderDbPxes");
            DropTable("dbo.AnswerDbPxes");
        }
    }
}
