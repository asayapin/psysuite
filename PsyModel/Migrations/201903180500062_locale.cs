namespace PsyModel.Migrations
{
    public partial class locale : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MethodicDbPxes", "Locale", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MethodicDbPxes", "Locale");
        }
    }
}
