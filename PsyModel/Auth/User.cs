﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using meet.common;
using meet.models.Annotations;
using meet.models.Db;
using meet.models.PassResults;

namespace meet.models.Auth
{

    public class User : Master, INotifyPropertyChanged
    {
        private string _identifier;

        public User()
        {
            
        }

        public User(string id, string name, UserRole role) : this()
        {
            Identifier = id;
            Name = name;
            Role = role;
        }

        /// <summary>
        /// used to identify user or client (e.g. psyClient-osmu-0)
        /// </summary>
        public string Identifier
        {
            get => _identifier;
            set
            {
                if (value == _identifier) return;
                _identifier = value;
                OnPropertyChanged();
            }
        }

        public string Name { get; set; }
        public UserRole Role { get; set; }

        public override string ToString()
        {
            return $"{Identifier} {Name}";
        }
        public virtual List<MethodicResult> Results { get; set; } = new List<MethodicResult>();
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}