﻿using System;
using System.Linq;

namespace meet.models
{
    public class Resources
    {
        public static string itemConst_drawingAnswer { get; set; }
        public static string error_Answer_conversionFailure { get; set; }
        public static string itemConst_picAnswer { get; set; }
        public static string itemConst_freeAnswer { get; set; }
        public static string format_MethodicPackProxy_TS { get; set; }
        public static string format_PlainRepresentation_TS { get; set; }
        public static string format_PlainRepresentation_build { get; set; }
        public static string const_blankTS { get; set; }
        public static string format_GraphicalRepresentation_TS { get; set; }
        public static string title_GraphicalRepresentation_charCtxSave { get; set; }
        public static string filter_GraphicalRepresentation_savePics { get; set; }
        public static string format_GraphicalRepresentation_saveTitle { get; set; }
        public static string format_ResultAnswer_freeTS { get; set; }
        public static string format_Patient_TS { get; set; }

        public static void Load(Type src)
        {
            foreach (var field in typeof(Resources).GetProperties())
            {
                var prop = src.GetProperties().FirstOrDefault(x => x.Name == field.Name);
                if (prop is null) continue;
                field.SetValue(null, prop.GetValue(null));
            }
        }
    }
}
