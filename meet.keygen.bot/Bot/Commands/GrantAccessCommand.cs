﻿using System.Linq;
using System.Threading.Tasks;
using meet.common;
using meet.keygen.bot.Controllers;
using Newtonsoft.Json;
using srs.Bots.Attributes;
using srs.Bots.Data;

namespace meet.keygen.bot.Bot.Commands
{
    [Command("grant", -1)]
    public class GrantAccessCommand: Command
    {
        public override async Task<PeerMessage> ProcessCommand(CommandContext context, ChatInfo chat)
        {
#if DEBUG
            Dependencies.Executor.InvokeMessageCreated(PeerMessage.FromText(JsonConvert.SerializeObject(context.Get()), KeygenBotController.Admin));
            Dependencies.Executor.InvokeMessageCreated(PeerMessage.FromText(JsonConvert.SerializeObject(context.Get(false)), KeygenBotController.Admin)); 
#endif

            if (chat.Id != KeygenBotController.Admin.Id) return PeerMessage.FromText("Access restricted", chat);
            var service = Dependencies.UsersService;
            if (context.Count() == 0)
                return PeerMessage.FromText("Select user", chat).WithOptions((await service.GetPendingUsers()).Select(x => x.ToString()), context);
            if (context.Count() == 1)
                return PeerMessage.FromText($"Set access levels: {UserRole.Admin} = {1 << (int)UserRole.Admin}, {UserRole.Doctor} = {1 << (int)UserRole.Doctor}, {UserRole.Patient} = {1 << (int)UserRole.Patient}", chat);
            if (context.Count() == 2)
                return PeerMessage.FromText("Set associated user (demo = 2)", chat);
            var userId = long.Parse(context[0]);
            var access = int.Parse(context[1]);
            var user = int.Parse(context[2]);
#if DEBUG
            Dependencies.Executor.InvokeMessageCreated(PeerMessage.FromText($"grant: {access} for {userId}", KeygenBotController.Admin)); 
#endif
            await service.SetRights(userId, access, user);
            KeygenBotController.UpdateUser(userId, access, false);
            context.SetProcessed();
            return PeerMessage.FromText("success", chat);
        }
    }
}
