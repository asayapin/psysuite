﻿using srs.Bots.CommandAbstractions;

namespace meet.keygen.bot.Bot.Commands
{
    public abstract class Command : srs.Bots.CommandAbstractions.Command
    {
        protected new Dependencies Dependencies;
        public override srs.Bots.CommandAbstractions.Command SetDependencies(CommandDependencies dep)
        {
            Dependencies= dep as Dependencies;
            return this;
        }
    }
}
