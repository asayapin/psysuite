﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using meet.common;
using meet.keygen.bot.Controllers;
using srs.Bots.Attributes;
using srs.Bots.Data;

namespace meet.keygen.bot.Bot.Commands
{
    [Command("пароль", -1)]
    public class CreateKeyCommand : Command
    {
        private static readonly Dictionary<UserRole, string> Roles = new Dictionary<UserRole, string>
        {
            [UserRole.Admin] = "администратор",
            [UserRole.Doctor] = "доктор",
            [UserRole.Patient] = "пациент"
        };

        public override Task<PeerMessage> ProcessCommand(CommandContext context, ChatInfo chat)
        {
            const string durationRequest = "Введите срок действия пароля в часах (не больше 12)";
            const string passwordResponse = "Ваш пароль: {0}";
            if (!KeygenBotController.GetUser(chat.Id, out var user) || user.PendingGrant || user.PermittedRoles == 0)
                return PeerMessage.FromText("Запрос на регистрацию ещё не одобрен", chat).AsTask();
            var roles = Convert.ToString(user.PermittedRoles, 2);
            if (context.Count() == 0)
            {
                if (roles.Count(x => x == '1') == 1)
                    return PeerMessage.FromText(durationRequest, chat).AsTask();
                var values = Roles.Where(x => ((1 << (int) x.Key) & user.PermittedRoles) != 0).Select(x => x.Value);
                return PeerMessage.FromText("Выберите уровень доступа", chat).WithOptions(values, context).AsTask();
            }
            if (context.Count() == 1)
            {
                if (roles.Count(x => x == '1') != 1) return PeerMessage.FromText(durationRequest, chat).AsTask();
                context.SetProcessed();
                return PeerMessage
                    .FromText(
                        string.Format(passwordResponse, CreateKey((UserRole) roles.Length, uint.Parse(context[1]), (uint)user.AssociatedUser)),
                        chat).AsTask();
            }

            context.SetProcessed();
            var role = Roles.FirstOrDefault(x => x.Value == context[0]).Key;
            return PeerMessage.FromText(string.Format(passwordResponse, CreateKey(role, uint.Parse(context[1]), (uint)user.AssociatedUser)), chat)
                .AsTask();
        }

        private static readonly Regex Split = new Regex("(.{1,4})");

        private static string CreateKey(UserRole role, uint duration, uint id)
        {
            return string.Join("-",
                Split.Split(role.CreateKey(id, duration).ToString()).Where(x => !string.IsNullOrWhiteSpace(x)));
        }
    }
}
