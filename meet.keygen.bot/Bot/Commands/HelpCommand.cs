﻿using System;
using System.Threading.Tasks;
using meet.keygen.bot.Controllers;
using srs.Bots.Attributes;
using srs.Bots.Data;

namespace meet.keygen.bot.Bot.Commands
{
    [Command("start"), Pseudonyms("помощь", "старт")]
    public class HelpCommand : Command
    {
        public override Task<PeerMessage> ProcessCommand(CommandContext context, ChatInfo chat)
        {
            const string notRegisteredResponse = "Вам пока недоступно создание паролей. Запрос на регистрацию отправлен администратору";
            const string commandsResponse = 
@"Доступные команды:
помощь - выводит это сообщение
пароль - создаёт новый пароль приложения

При создании пароля вам будет предложено выбрать:
- уровень доступа (администратор, доктор, пациент)
- длительность действия пароля (до 12 часов)";
            if (KeygenBotController.GetUser(chat.Id, out var user))
                return PeerMessage.FromText(user.PendingGrant || user.PermittedRoles == 0? notRegisteredResponse : commandsResponse, chat)
                    .AsTask();
            throw new InvalidOperationException($"user not found: {chat.Id}");
        }
    }
}
