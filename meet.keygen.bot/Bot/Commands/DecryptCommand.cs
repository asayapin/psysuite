﻿using System.Threading.Tasks;
using meet.common;
using meet.keygen.bot.Controllers;
using srs.Bots.Attributes;
using srs.Bots.Data;

namespace meet.keygen.bot.Bot.Commands
{
    [Command("decrypt", 1)]
    public class DecryptCommand : Command
    {
        public override Task<PeerMessage> ProcessCommand(CommandContext context, ChatInfo chat)
        {
            if (chat.Id != KeygenBotController.Admin.Id) return PeerMessage.FromText("Access restricted", chat).AsTask();
            if (context.Count() == 0) return PeerMessage.FromText("Enter key to decrypt", chat).AsTask();
            var key = ulong.Parse(context[0].Replace("-", "").Trim());
            var (userRole, dateTime, id) = key.DecryptKey();
            return PeerMessage.FromText($"role: {userRole}\nexpire on: {dateTime}\nid: {id}", chat).AsTask();
        }
    }
}
