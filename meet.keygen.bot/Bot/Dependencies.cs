﻿using meet.keygen.bot.DAL;
using srs.Bots.CommandAbstractions;

namespace meet.keygen.bot.Bot
{
    public class Dependencies : CommandDependencies
    {
        public Dependencies(UsersService usersService)
        {
            UsersService = usersService;
        }

        public UsersService UsersService { get; }
    }
}
