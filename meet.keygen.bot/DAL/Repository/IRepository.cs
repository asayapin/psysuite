﻿using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User = meet.keygen.bot.Models.User;

namespace meet.keygen.bot.DAL.Repository
{
    public interface IRepository<TEntity, TKey>
    {
        Task<IEnumerable<TEntity>> ReadAsync();

        Task<TEntity> GetAsync(TKey key);
        Task<TEntity> AddAsync(TEntity entity);
        Task<bool> UpdateAsync(TEntity entity);
    }

    public class UserCosmosDbRepository : IRepository<User, long>
    {
        private readonly CosmosClient _cosmosClient;
        private Database _cosmosDatabase;
        private Container _cosmosContainer;
        public UserCosmosDbRepository(IConfiguration config)
        {
            var uri = config["cosmosUri"];
            var key = config["cosmosKey"];
            var dbName = config["cosmosDbName"];
            var container = config["cosmosContainer"];

            _cosmosClient = new CosmosClient(uri, key);
            _cosmosClient
                .CreateDatabaseIfNotExistsAsync(dbName)
                .ContinueWith(x => {
                    _cosmosDatabase = x.Result.Database;
                    _cosmosContainer = _cosmosDatabase.GetContainer(container);
                })
                .GetAwaiter().GetResult();
        }
        public async Task<User> AddAsync(User entity)
        {
            return (await _cosmosContainer.CreateItemAsync(entity)).Resource;
        }

        public async Task<User> GetAsync(long key)
        {
            return (await _cosmosContainer.ReadItemAsync<User>(key.ToString(), new PartitionKey("/ChatId"))).Resource;
        }

        public Task<IEnumerable<User>> ReadAsync()
        {
            return Task.FromResult(_cosmosContainer.GetItemLinqQueryable<User>().ToList().AsEnumerable());
        }

        public async Task<bool> UpdateAsync(User entity)
        {
            return (await _cosmosContainer.UpsertItemAsync(entity)).StatusCode == System.Net.HttpStatusCode.OK;
        }
    }
}
