﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using meet.keygen.bot.DAL.Repository;
using meet.keygen.bot.Models;

namespace meet.keygen.bot.DAL
{
    public class UsersService
    {
        private readonly IRepository<User, long> _repository;

        public UsersService(IRepository<User, long> repository)
        {
            _repository = repository;
        }


        public async Task<User> GetOrCreateUser(long peerId, string name)
        {
            var user = await _repository.GetAsync(peerId);
            if (user is { }) return user;
            user = new User { 
                ChatId = peerId,
                ChatName = name
            };
            return await _repository.AddAsync(user);
        }

        public async Task<IEnumerable<long>> GetPendingUsers()
        {
            return (await _repository.ReadAsync()).Where(x => x.PendingGrant).Select(x => x.ChatId);
        }

        public async Task SetRights(long userId, int access, int user)
        {
            var userEntity = await _repository.GetAsync(userId);
            if (userEntity is null) return;
            userEntity.PendingGrant = false;
            userEntity.PermittedRoles = access;
            userEntity.AssociatedUser = user;
            await _repository.UpdateAsync(userEntity);
        }
    }
}
