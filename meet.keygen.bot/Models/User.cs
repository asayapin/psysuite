﻿namespace meet.keygen.bot.Models
{


    public class User
    {
        public long ChatId { get; set; }
        public int PermittedRoles { get; set; }
        public bool PendingGrant { get; set; }
        public int AssociatedUser { get; set; }
        public string ChatName { get; set; }
    }
}
