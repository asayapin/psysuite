﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using meet.keygen.bot.DAL;
using meet.keygen.bot.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using srs.Bots.CommandAbstractions;
using srs.Bots.ControllerAbstractions;
using srs.Bots.Data;
using srs.Bots.tlg;

namespace meet.keygen.bot.Controllers
{
    [ApiController]
    [Route("api/bot")]
    public class KeygenBotController : TelegramControllerBase
    {
        static KeygenBotController()
        {
            BotTokenKey = "botAccessToken";
            InlineMismatchFormat = "{0} не является корректной командой";
            QuickAnswer = "Бот загружается, подождите";
        }

        internal static ChatInfo Admin;
        private static readonly ConcurrentDictionary<long, User> Users = new ConcurrentDictionary<long, User>();

        internal static bool GetUser(long peerId, out User user) => Users.TryGetValue(peerId, out user);

        private readonly UsersService _usersService;

        public KeygenBotController(ControllerSetup setup, Executor executor, IConfiguration config, UsersService usersService) : base(setup, executor, config)
        {
            _usersService = usersService;
            Admin = ChatInfo.FromId(config.GetValue<long>("botAdminChat"));
        }

        [HttpPost]
        public Task<IActionResult> Post(JObject request)
        {
#if DEBUG
            Executor.InvokeMessageCreated(PeerMessage.FromText(request.ToString(), Admin)); 
#endif
            return PostHandler(request);
        }

        protected override void PreProcessMessage(UnifiedMessage message)
        {
            try
            {
                if (!Users.ContainsKey(message.Peer.Id))
                {
                    var user = _usersService.GetOrCreateUser(message.Peer.Id, message.Peer.Name).Result;
                    Users[user.ChatId] = user;
                    if (user.PendingGrant)
                        Client.SendPeerMessageAsync(PeerMessage.FromText($"new pending grant: {message.Peer.Id} ({message.Peer.Name})", Admin)).Wait();
                }
                base.PreProcessMessage(message);
            }
            catch (Exception ex) { LogException(ex); }
        }

        protected override Task FallbackHandler(UnifiedMessage message) => Task.CompletedTask;

        protected override void LogException(Exception ex)
        {
            Client.SendPeerMessageAsync(PeerMessage.FromText($"{DateTime.UtcNow}\n{ex.Message}\n{ex.StackTrace}", Admin)).Wait();
        }

        public static void UpdateUser(long userId, int access, bool pending)
        {
            if (Users.TryGetValue(userId, out var user))
            {
                user.PermittedRoles = access;
                user.PendingGrant = pending;
            }
            else Users[userId] = new User{ChatId = userId, PermittedRoles = access, PendingGrant = pending};
        }

        protected override async Task<bool> ProcessSpecialMessages(UnifiedMessage message)
        {
            switch (message.Text.ToLower())
            {
                case "стоп":
                case "stop":
                    TryRemoveContext(message.Peer.Id, out _);
                    Executor.InvokeMessageCreated(PeerMessage.FromText("Команда отменена", message.Peer));
                    return true;
            }
            return await base.ProcessSpecialMessages(message);
        }
    }
}
