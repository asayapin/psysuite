using System;
using NUnit.Framework;

namespace meet.common.tests
{
    public class KeysTests
    {
        [TestCase(UserRole.Patient, 1u, 2u)]
        [TestCase(UserRole.Patient, 18u, 2u)]
        [TestCase(UserRole.Doctor, 1u, 2u)]
        [TestCase(UserRole.Doctor, 18u, 2u)]
        [TestCase(UserRole.Admin, 1u, 2u)]
        [TestCase(UserRole.Admin, 18u, 2u)]
        public void TestRoundTrip(UserRole role, uint id, uint duration)
        {
            var enc = role.CreateKey(id, duration);
            var (r, d, i) = enc.DecryptKey();

            Console.WriteLine("{0} {1} {2}", r, i, d);

            Assert.IsTrue(r == role);
            Assert.IsTrue(i == id);
            Assert.IsTrue((d - TimeSpan.FromHours(duration)) - DateTime.UtcNow <= TimeSpan.FromMinutes(15));
        }
    }
}