FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /meet.keygen.bot
COPY /pub/. ./
ENTRYPOINT ["dotnet", "meet.keygen.bot.dll"]